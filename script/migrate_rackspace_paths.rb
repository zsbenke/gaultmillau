def update_blocks(old_path, new_path)
  Block.where("content ilike ?", "%#{old_path}%").find_each do |block|
    content = block.content.gsub(old_path, new_path)
    block.update_attribute :content, content
    puts "Updated block ##{block.id}"
  end
end

old_path = "http://7405ba8ce3f6dfa3c5a6-6d1814b92d4b4199228d63447726e19d.r77.cf1.rackcdn.com"
new_path = "http://gaultmillau.hu/system"
update_blocks(old_path, new_path)

old_path = "http://8771d7f8b1ad39ce3984-0b66c1423c75f2ffcd95ba500240fa00.r32.cf1.rackcdn.com"
new_path = "http://gaultmillau.hu/system"
update_blocks(old_path, new_path)

old_path = "http://688daa9df942e9a1e516-0015e312a498ca58307a8a1849804af4.r48.cf1.rackcdn.com"
new_path = "http://assets.gaultmillau.hu/system"
update_blocks(old_path, new_path)