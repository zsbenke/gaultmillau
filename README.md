### Gault&Millau évváltás (GM)

1. A [gaultmillau_preferences.rb](https://github.com/wyctim/gaultmillau/blob/master/config/initializers/gaultmillau_preferences.rb) fájlban a `CONFIG_GMDB_YEAR` konstans beállítása új tesztévre
2. Deploy
3. `rake admin:import_redeem_codes` futattása - kitörli a tavalyi kuponkódokat és beimportálja a [redeem_codes.csv](https://github.com/wyctim/gaultmillau/blob/master/lib/tasks/csv/redeem_codes.csv) tartalmát. Új kuponkódok a `rake admin:generate_redeem_codes` scripttel generálhatók.
4. `rake admin:migrate_favourites` futtatása - migrálja a kedvenceket az idei rekordokra. Ha a rekord már nem elérhető, akkor törli a favot.
5. Cache törlése a `rake admin:clear_cache` scripttel

### Egyéb átállással kapcsolatos tennivalók

1. zászlóbeállítások programból (tipikusan éttermeknél: 1-es zászlóra azt, ami az előző évben megjelent, 9-es zászlóra azt, ami korábban is 9-es volt, 10-es zászlóra, ami korábban is 10-es volt, szállodáknál és borászatoknál 1-es zászlóra azt, ami az előző évben megjelent)
2. prioritás táblázatok betöltése (mindhárom modulban pontszám szerint csökkenő, azon belül betűrendi sorrend, ettől a kereséseknél prioritások szerint jelennek meg az eredmények, tehát a T betűsök később jönnek, mint a jó éttermek stb.)
