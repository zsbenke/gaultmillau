class ProductPolicy
  attr_reader :user, :product

  def initialize(user, post)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @product = product
  end

  def index?
    user.role_admin
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
  alias_method :destroy?, :index?
end
