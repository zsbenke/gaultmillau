class HomeBlockPolicy
  attr_reader :user, :home_block

  def initialize(user, home_block)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @home_block = home_block
  end

  def index?
    user.role_admin || user.role_senior_editor
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
  alias_method :destroy?, :index?
end
