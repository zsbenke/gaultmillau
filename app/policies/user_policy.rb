class UserPolicy
  attr_reader :user, :user_record

  def initialize(user, user_record)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @user_record = user_record
  end

  def index?
    user.role_admin
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
  alias_method :destroy?, :index?
  alias_method :manual_confirm?, :index?
end
