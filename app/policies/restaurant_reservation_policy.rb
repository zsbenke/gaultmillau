class RestaurantReservationPolicy
  attr_reader :user, :restaurant_reservation

  def initialize(user, restaurant_reservation)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @restaurant_reservation = restaurant_reservation
  end

  def index?
    user.role_admin
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
  alias_method :destroy?, :index?
end
