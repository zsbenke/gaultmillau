class OrderPolicy
  attr_reader :user, :order

  def initialize(user, order)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @order = order
  end

  def index?
    user.role_admin || user.role_accountant
  end

  def destroy?
    user.role_admin
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
end
