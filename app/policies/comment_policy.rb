class CommentPolicy
  attr_reader :user, :comment

  def initialize(user, post)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @comment = comment
  end

  def index?
    user.role_admin || user.role_senior_editor || user.role_editor
  end

  alias_method :show?, :index?
  alias_method :new?, :index?
  alias_method :edit?, :index?
  alias_method :create?, :index?
  alias_method :update?, :index?
  alias_method :destroy?, :index?
end
