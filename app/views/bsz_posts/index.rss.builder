xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0", "xmlns:atom" => 'http://www.w3.org/2005/Atom' do
  xml.channel do
    xml.title t('bsz.name')
    xml.description t('bsz.name')
    xml.link bsz_posts_url
    xml.language "hu"
    xml.tag! "atom:link", :href => bsz_feed_url, :rel => "self", :type => "application/rss+xml"

    for post in @posts
      xml.item do
        xml.title post.title
        xml.description post.excerpt
        xml.pubDate post.published_at.to_s(:rfc822)
        xml.link get_post_url_for(post)
        xml.guid get_post_url_for(post)
      end
    end
  end
end
