atom_feed :language => 'hu-HU' do |feed|
  feed.title("#{t('news.name')} - #{t('comment.name')}")
  feed.updated(@comments[0].updated_at) if @comments.length > 0

  @comments.each do |comment|
    feed.entry(comment, :url => news_url(comment.post.url)) do |entry|
      entry.title comment.post.title
      entry.content "#{comment.body} […]", :type => 'html'

      entry.author do |author|
        author.name(comment.post.user.username)
      end
    end
  end
end
