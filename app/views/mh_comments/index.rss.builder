xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0", "xmlns:atom" => 'http://www.w3.org/2005/Atom' do
  xml.channel do
    xml.title "#{t('mh.name')} - #{t('comment.name')}"
    xml.description "#{t('mh.name')} - #{t('comment.name')}"
    xml.link mh_posts_url
    xml.language "hu"
    xml.tag! "atom:link", :href => mh_comments_feed_url, :rel => "self", :type => "application/rss+xml"

    for comment in @comments
      xml.item do
        xml.title "#{comment.post.title} – #{comment.user.username}"
        xml.description comment.body
        xml.pubDate comment.created_at.to_s(:rfc822)
        xml.link "#{get_post_url_for(comment.post)}#comment-#{comment.id}"
        xml.guid "#{get_post_url_for(comment.post)}#comment-#{comment.id}"
      end
    end
  end
end
