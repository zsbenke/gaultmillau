class RestaurantReservationMailer < ActionMailer::Base
  default from: CONFIG_DEFAULT_FROM

  def restaurant_reservation_booking(restaurant_reservation)
    @restaurant_reservation = restaurant_reservation
    mail(:to => @restaurant_reservation.restaurant.email, :subject => "#{CONFIG_SITE_NAME}: Asztalfoglalás")
  end

  def restaurant_reservation_confirmed(restaurant_reservation)
    @restaurant_reservation = restaurant_reservation
    mail(:to => @restaurant_reservation.restaurant.email, :subject => "#{CONFIG_SITE_NAME}: Asztalfoglalás visszaigazolva")
  end
end
