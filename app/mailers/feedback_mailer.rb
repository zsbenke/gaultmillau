class FeedbackMailer < ActionMailer::Base
  default from: CONFIG_DEFAULT_FROM
  add_template_helper(ApplicationHelper)

  def contact(feedback, remote_ip)
    @feedback = feedback
    @remote_ip = remote_ip
    mail(:reply_to => @feedback.address, :from => "support@gaultmillau.hu", :to => CONFIG_PL_ADDRESS, :bcc => "support@gaultmillau.hu", :subject => "Gault&Millau: Visszajelzés a kapcsolat oldalról")
  end
end
