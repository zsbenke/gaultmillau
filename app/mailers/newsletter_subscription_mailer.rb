class NewsletterSubscriptionMailer < ActionMailer::Base
  default from: CONFIG_DEFAULT_FROM

  def newsletter_subscription_confirmation(newsletter_subscription)
    @newsletter_subscription = newsletter_subscription
    mail(:to => @newsletter_subscription.email, :subject => "#{CONFIG_SITE_NAME}: Hírlevél feliratkozás")
  end
end
