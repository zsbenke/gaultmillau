class OrderMailer < ActionMailer::Base
  default from: CONFIG_DEFAULT_FROM

  def order_confirmation(order)
    @order = order
    mail(:to => @order.email, :subject => "#{CONFIG_SITE_NAME}: #{@order.product.name} megrendelése")
  end
end
