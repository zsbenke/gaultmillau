class RedeemCode < ActiveRecord::Base
  belongs_to :user
  validates_uniqueness_of :user_id

  def belongs_to_a_user?
    !self.user_id.nil?
  end
end
