class Hotel < GmdbConnection
  include Filterable
  self.table_name = 'hotels'

  RECORD_KIND = ['Tesztelt szállodák', 'Ajánlott helyek']

  has_many :hotel_images

  has_one :child, :class_name => "Hotel", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Hotel", :foreign_key => "parent_id"

  default_scope { order('position ASC') }

  include PgSearch
  pg_search_scope :filter_by_keyword, against: [:search_cache], :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :public, lambda { where(:public => true, :status => 'Lezárt anyag', :year => CONFIG_GM_YEAR) }
  scope :by_record_kind, lambda { |record_kind| where(:record_kind => record_kind) unless record_kind.blank? }
  scope :by_rating, lambda { |rating|
    return unless rating.present? & rating.kind_of?(Array)
    possible_ratings = %w(1 2 3 4 5)

    filtered_ratings = []
    rating.each do |r|
      if (possible_ratings.include?(r))
        filtered_ratings << r
      end
    end
    filtered_ratings = filtered_ratings.flatten
    where(:rating => filtered_ratings)
  }

  def self.by_keyword(keyword)
    if !keyword.blank?
      filter_by_keyword(keyword)
    else
      order('hotels.created_at DESC')
    end
  end

  def record_kind_class
    case record_kind
    when 'Ajánlott helyek' then 'recommended'
    else 'tested'
    end
  end
end
