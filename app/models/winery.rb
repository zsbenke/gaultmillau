class Winery < GmdbConnection
  include Filterable
  self.table_name = 'wineries'

  has_many :winery_images
  has_many :wines

  has_one :child, :class_name => "Winery", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Winery", :foreign_key => "parent_id"

  default_scope {  order('position ASC') }

  include PgSearch
  pg_search_scope :filter_by_keyword, against: [:search_cache], :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :public, lambda { where(:public => true, :status => 'Lezárt anyag', :year => CONFIG_GM_YEAR) }
  scope :by_rating, lambda { |rating|
    return unless rating.present? & rating.kind_of?(Array)
    predefined_ratings = {
      'best_of' => %w(4, 5, 3)
    }
    possible_ratings = %w(1 2 3 4 5)

    filtered_ratings = []
    rating.each do |r|
      if predefined_ratings.keys.include?(r)
        filtered_ratings << predefined_ratings[r]
      elsif (possible_ratings.include?(r))
        filtered_ratings << r
      end
    end
    filtered_ratings = filtered_ratings.flatten
    where(:rating => filtered_ratings)
  }
  scope :by_region, lambda { |region| where(:region => region) unless region.blank? }
  scope :by_wines_rating, lambda { |rating|
    joins(:wines).where(:wines => { :rating => rating.sub(' pont', '') }) unless rating.blank?
  }
  scope :by_wines_grape, lambda { |grape|
    wine_ids = []
    if !grape.blank?
      grape_tag = GrapeTag.by_name grape
      if !grape_tag.nil?
        grape_taggings = GrapeTagging.where :tag_id => grape_tag.id

        grape_taggings.each do |tagging|
          wine_ids << tagging.taggable_id
        end

        joins(:wines).where(:wines => { :id => wine_ids })
      else
        joins(:wines).where(:wines => { :id => nil })
      end
    end
  }
  scope :by_wines_genre, lambda { |genre| joins(:wines).where(:wines => { :genre => genre } ) unless genre.blank? }

  def self.by_keyword(keyword)
    if !keyword.blank?
      filter_by_keyword(keyword)
    else
      order('wineries.created_at DESC')
    end
  end

  def self.winery_regions
    ['Tokaj-hegyaljai',
    'Bükkaljai',
    'Egri',
    'Mátraaljai',
    'Kiskunsági',
    'Hajós-Bajai',
    'Szekszárdi',
    'Villányi',
    'Mecsekaljai',
    'Etyek-Budai',
    'Ászár-Neszmélyi',
    'Móri',
    'Balatonfüred-Csopaki',
    'Badacsonyi',
    'Határon túli',
    'Zalai',
    'Somlói',
    'Pannonhalmai',
    'Soproni',
    'Dél-balatoni']
  end
end
