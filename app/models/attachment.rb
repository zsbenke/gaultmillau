class Attachment < ActiveRecord::Base
  has_many :blocks
  has_many :posts
  validates :attachment, :category, :presence => true

  has_attached_file :attachment, :styles => { :small => "200x200>", :medium => "1500x10000>" }
  process_in_background :attachment, :processing_image_url => "/assets/processing.png"

  validates_attachment_size :attachment, :less_than => 10.megabytes
  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'application/pdf', 'application/msword']

  default_scope { order("created_at DESC") }
  scope :by_categories, lambda { |category|
    if category == 'latest'
      where(:created_at => Date.today) unless category.blank?
    else
      where(:category => category) unless category.blank?
    end
  }
  scope :by_empty_category, lambda { where('category is null OR category = ?', "") }
  scope :by_keyword, lambda { |keyword| where('title ilike ?', "%#{keyword}%") unless keyword.blank? }

  before_post_process :only_images
  before_create :default_name, :get_average_color

  def only_images
    %w(image/jpeg image/png image/gif).include?(attachment_content_type)
  end

  def default_name
    self.title ||= self.attachment_file_name
  end

  def get_average_color
    if %w(image/jpeg image/png image/gif).include?(attachment_content_type)
      tempfile = attachment.queued_for_write[:original]
      image = Magick::Image.read(tempfile.path).first
      if image.alpha?
        self.average_color = "transparent"
      else
        self.average_color = calculate_average_color image
      end
    end
  end


  def calculate_average_color image
    total = 0
    avg = { :r => 0.0, :g => 0.0, :b => 0.0 }

    image.quantize.color_histogram.each do |c,n|
      avg[:r] += n * c.red
      avg[:g] += n * c.green
      avg[:b] += n * c.blue
      total += n
    end

    avg.each_key do |c|
      avg[c] /= total
      avg[c] = (avg[c] / Magick::QuantumRange * 255).to_i
    end

    return "rgb(#{avg[:r]},#{avg[:g]},#{avg[:b]})"
  end

  def self.maximum_file_size
    10.megabytes
  end

  def self.categories
    [['Művelt Alkoholista', 'a-muvelt-alkoholista'],
     ['Bűvös Szakács', 'buvos-szakacs'],
     ['Mese Habbal', 'mese-habbal'],
     ['G&M Magazin', 'hirek'],
     ['Receptek', 'receptek'],
     ['Címlapfotók', 'cimlapfotok'],
     ['Oldalak', 'oldalak'],
     ['Egyéb', 'egyeb']]
  end

  def self.reprocess_attachments!
    Attachment.find_each do |a|
      begin
        a.attachment.reprocess!
        puts "Scheduled attachment #{a.id} for reprocessing"
      rescue
        puts "Skipped attachment #{a.id} while reprocessing"
        next
      end
    end
  end
end
