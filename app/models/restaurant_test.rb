class RestaurantTest < ActiveRecord::Base
  establish_connection(ENV['GMDB_DATABASE_URL'])
  self.table_name = 'restaurant_tests'

  belongs_to :restaurant
  scope :public, lambda { where('status = ?', 'Lezárt anyag') }


  def self.options_for_rating
    ['T', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
  end

  def print_as_formatted
    print.gsub("\r", "\n")
  end
end
