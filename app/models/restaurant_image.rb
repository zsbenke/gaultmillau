class RestaurantImage < ActiveRecord::Base
  establish_connection(ENV['GMDB_DATABASE_URL'])
  self.table_name = 'restaurant_images'

  belongs_to :restaurant

  has_attached_file :restaurant_image, :styles => { :large => "800x600>", :medium => "300x300>", :thumb => "100x100>" }
  scope :paid, lambda { where(:paid => true) }
end
