class NewsletterSubscription < ActiveRecord::Base
  include Tokenable
  belongs_to :user
  # attr_accessible :email, :token, :confirmed, :confirmed_at
  validates :email, :presence => true
  validates_format_of :email, :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/, :message => I18n.t(:invalid)
  validates_uniqueness_of :token, :email

  before_create :generate_token
end
