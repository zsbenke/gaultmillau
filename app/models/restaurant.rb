class Restaurant < GmdbConnection
  include Filterable
  include Openable

  self.table_name = 'restaurants'

  has_many :restaurant_tests
  has_many :restaurant_images
  has_many :restaurant_reservations

  has_one :child, :class_name => "Restaurant", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Restaurant", :foreign_key => "parent_id"

  default_scope { order('position ASC') }

  include PgSearch
  pg_search_scope :filter_by_keyword, against: [:search_cache, :tags_index], :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :public, lambda { where("restaurants.public = ? AND restaurants.year = ?", true, CONFIG_GM_YEAR) }
  scope :by_open_on_monday, lambda { |open_on_monday| where(:open_on_monday => open_on_monday) unless open_on_monday.blank? }
  scope :by_open_on_sunday, lambda { |open_on_sunday| where(:open_on_sunday => open_on_sunday) unless open_on_sunday.blank? }
  scope :by_rating, lambda { |rating|
    return unless rating.present? & rating.kind_of?(Array)
    predefined_ratings = {
      'toque_3' => %w(17 18),
      'toque_2' => %w(16 15),
      'toque_1' => %w(14 13),
      'recommended' => %w(12 11),
      'lower' => %w(10 9 8 T),
      'best_of' => %w(11 12 13 14 15 16 17 18)
    }

    possible_ratings = %w(T 8 9 10 11 12 13 14 15 16 17 18 19 20)

    filtered_ratings = []
    rating.each do |r|
      if predefined_ratings.keys.include?(r)
        filtered_ratings << predefined_ratings[r]
      elsif (possible_ratings.include?(r))
        filtered_ratings << r
      end
    end
    filtered_ratings = filtered_ratings.flatten
    sql = 'restaurant_tests.status = ? AND restaurant_tests.rating IN (?)'
    rating_ids = Restaurant.unscoped.public.joins(:restaurant_tests).where(sql, 'Lezárt anyag', filtered_ratings).pluck(:id)
    pop_ids = []
    if rating.include?('pop')
      pop_ids = Restaurant.unscoped.public.where(pop: true).pluck(:id)
    end
    return where(id: (rating_ids + pop_ids).flatten.uniq)
  }
  scope :by_tags, lambda { |tag| where('restaurants.tags_index ilike ?', "%#{tag}%") unless tag.blank? }

  def self.by_keyword(keyword)
    if !keyword.blank?
      filter_by_keyword(keyword)
    else
      order('restaurants.created_at DESC')
    end
  end

  def self.cached_public
    Rails.cache.fetch([self, "public"], expires_in: 5.minutes) do
      public
    end
  end
end
