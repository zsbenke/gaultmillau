class WineryImage < ActiveRecord::Base
  establish_connection(ENV['GMDB_DATABASE_URL'])
  self.table_name = 'winery_images'

  belongs_to :winery
  has_attached_file :winery_image,
                    :styles => { :large => "800x600>", :medium => "300x300>", :thumb => "100x100>" }

  scope :paid, lambda { where(:paid => true) }
end
