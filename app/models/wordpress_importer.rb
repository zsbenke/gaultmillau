class WordpressImporter
  attr_reader :client, :posts, :categories

  RawPost = Struct.new(:title, :slug, :tags, :date, :body, :photos, :comments, :status)
  RawComment = Struct.new(:id, :author, :email, :date, :body)
  Photo = Struct.new(:basename, :src)

  def initialize(host, username, password, export_dir)
    raise 'export_dir has to be a Pathname' unless export_dir.is_a?(Pathname)
    @export_dir = export_dir
    @client = Rubypress::Client.new(:host => host, username: username, password: password)
    @categories = @client.getTerms.select { |term| term['taxonomy'] == 'category' }.map do |category|
      {
        term_id: category['term_id'],
        name: category['name'],
        tag_name: category['name'].mb_chars.downcase.to_s
      }
    end
    puts "Got categories: #{@categories.map { |c| c['name'] }}"
  end

  def post
    parse

    per_page = 100
    posts_offset = 0
    current_batch_count = nil
    downloaded_posts = []

    while current_batch_count != 0 do
      posts = @client.getPosts(filter: { number: per_page, offset: posts_offset })
      downloaded_posts << posts
      puts "Downloaded existing posts by offset: #{posts_offset}"

      posts_offset += per_page
      current_batch_count = posts.count
    end

    downloaded_posts = downloaded_posts.flatten.map { |p| p['post_name'] }
    posts_count = @posts.count

    @posts.each_with_index do |post, i|
      uploaded_post = nil

      if downloaded_posts.include?(post.slug)
        puts "Already exists: #{post.title} (#{i} of #{posts_count})"
        next
      end
      uploaded_photos = []

      write_log "Started importing post: #{post.title} (#{i} of #{posts_count})"

      post.photos.each do |photo|
        filename = @export_dir.join('images', post.slug, photo).to_s
        if File.exist?(filename)
          write_log "Started uploading photo: #{filename}"
          begin
            uploaded_photo = @client.uploadFile(data: {
              name: photo,
              type: MIME::Types.type_for(filename).first.to_s,
              bits: XMLRPC::Base64.new(IO.read(filename))
            })
            link = uploaded_photo['link']
            uploaded_photos << Photo.new(photo, link)
          rescue
            write_log "Failed uploading photo: #{filename}"
          end
        else
          write_log "Photo not found: #{filename}"
        end

        post.body.css('img').each do |img|
          basename = File.basename(img[:src])
          uploaded_photo = uploaded_photos.select { |photo| photo.basename == basename }.last
          if uploaded_photo.present?
            uploaded_photo_src = uploaded_photo.src.gsub("http://#{@client.host}", '')
            img[:src] = uploaded_photo_src if uploaded_photo.present?
          end
        end
      end

      post.body.css('figure.centered img').each do |img|
        classes = ''
        classes = img[:class] if img[:class].present?
        classes << ' aligncenter'
        img[:class] = classes
      end

      post.body.css('.embedded-recipe').each do |recipe|
        recipe_slug = recipe['data-recipe-url']
        recipe.content = "[insert page='#{recipe_slug}' display='all']"
      end

      content = {
        post_status: post.status,
        post_date_gmt:  post.date - 2.hours, #adjustment for GMT timezone
        post_content: post.body.inner_html,
        post_title: post.title,
        post_name: post.slug,
        post_author: 1,
        comment_status: 'open'
      }

      if post.tags.any?
        content['terms_names'] = {
          category: categories_with_tags(post),
          post_tag: post.tags
        }
      else
        content['terms_names'] = {
          category: categories_with_tags(post)
        }
      end

      begin
        uploaded_post = @client.newPost(
          blog_id: 0,
          content: content
        )

        write_log "Posted: #{post.title}"
      rescue
        write_log "Failed posting: #{post.title}"
        next
      end

      post.comments.each do |comment|
        post_id = uploaded_post

        comment_hash = {
          author: comment.author,
          author_email: comment.email,
          content: comment.body
        }

        begin
          comment_id = client.newComment(
            post_id: post_id,
            comment: comment_hash
          )

          comment_hash['status'] = 'approve'
          comment_hash['date_created_gmt'] = comment.date - 2.hours # adjustment for GMT timezone

          edited_comment = client.editComment(
            comment_id: comment_id,
            comment: comment_hash
          )
          write_log "Posted comment #{comment.id} for #{post.title}"
        rescue
          write_log "Failed posting comment: #{comment.id}"
          next
        end
      end
    end
    nil
  end

  def parse
    @posts = []
    Dir.entries(@export_dir).select { |f| f.match /\.html/ }.each do |post_name|
      begin
        post = parse_post(post_name)
        @posts.append(post)
        write_log "Parsed post: #{post.title}"
      rescue
        write_log "Skipping post: #{post_name}"
      end
    end
  end

  def parse_post(post_name)
    file = File.open(@export_dir.join(post_name))
    nodes = Nokogiri::HTML(file.read)

    post_title = nodes.at_css('.post-header h1 a').content
    post_slug = post_name.gsub('.html', '')
    post_tags = nodes.css('.post-header .post-tags-popover li a').map(&:content)
    if nodes.at_css('.post')['data-tags'].present?
      post_tags = nodes.at_css('.post')['data-tags'].split(', ')
    end
    post_date = parse_time nodes.at_css('.post-meta .post-date').content
    post_body = nodes.at_css('.post-body')
    post_photos = Dir.entries(@export_dir.join('images', post_slug)).
      reject { |i| ['.', '..'].include? i }
    post_comments = nodes.at_css('#comments').css('.comment').map do |comment_node|
      comment_id = comment_node['data-comment-id']
      comment_author = comment_node['data-comment-username']
      comment_email = comment_node['data-author-email']
      comment_date = parse_time(comment_node.at_css('.comment-title .subtitle a').content.gsub('#', ''))

      comment_node.at_css('.comment-title').remove
      comment_body = comment_node.inner_html

      RawComment.new(comment_id, comment_author, comment_email, comment_date, comment_body)
    end
    post_status = nodes.at_css('body')[:class]
    post_status = 'publish' if post_status == 'public'

    post_body.at_css('.share-actions').remove if post_body.at_css('.share-actions').present?

    post = RawPost.new(
      post_title,
      post_slug,
      post_tags,
      post_date,
      post_body,
      post_photos,
      post_comments,
      post_status
    )

    return post
  end

  def categories_with_tags(post)
    categories_and_tags = [category_name]

    post.tags.each do |tag|
      if category = @categories.find { |category| category[:tag_name] == tag }
        categories_and_tags << category[:name]
      end
    end

    categories_and_tags.delete(category_name) if categories_and_tags.count > 1
    categories_and_tags
  end

  def category_name
    category_hash = Hash.new.tap { |h| Post.categories.each { |v, k| h[k] = v } }
    category_hash[File.basename(@export_dir)]
  end

  private

  def parse_time(raw)
    months = {
      'január' => 'January',
      'február' => 'February',
      'március' => 'March',
      'április' => 'April',
      'május' => 'May',
      'június' => 'June',
      'július' => 'July',
      'augusztus' => 'August',
      'szeptember' => 'September',
      'október' => 'October',
      'november' => 'November',
      'december' => 'December'
    }

    months.keys.each do |month|
      raw.gsub!(month, months[month])
    end

    return DateTime.strptime(raw, "%Y. %B %d. %H:%M")
  end

  def write_log(msg)
    import_log_file = @export_dir.join('wordpress_import.log')
    File.open(import_log_file, 'a+') { |file| file.write(Time.now.strftime("%Y-%m-%d %H:%M:%S - ") + "#{msg}\n") }
    puts msg
  end

end
