class Wine < GmdbConnection
  self.table_name = 'wines'

  belongs_to :winery
  has_many :wine_images
  has_many :grape_taggings, :foreign_key => 'taggable_id'

  default_scope {  order('wines.best_buy DESC') }

  scope :by_rating, lambda { |rating| where(:rating => rating.sub(' pont', '')) unless rating.blank? }
  scope :by_grape, lambda { |grape|
    wine_ids = []
    if !grape.blank?
      grape_tag = GrapeTag.by_name grape
      if !grape_tag.nil?
        grape_taggings = GrapeTagging.where :tag_id => grape_tag.id

        grape_taggings.each do |tagging|
          wine_ids << tagging.taggable_id
        end

        where(:id => wine_ids)
      else
        where(:id => nil)
      end
    end
  }
  scope :by_genre, lambda { |genre| where(:genre => genre) unless genre.blank? }

  def self.by_keyword(keyword)
    if !keyword.blank?
      filter_by_keyword(keyword)
    else
      order('wines.created_at DESC')
    end
  end

  def self.rating_options
    ['17 pont', '16 pont', '15 pont', '14 pont', '13 pont', '12 pont', '11 pont', '10 pont', '9 pont', '8 pont', 'T']
  end

  def self.grape_options
    ['furmint',
     'cuvée',
     'hárslevelű',
     'kékfrankos',
     'cabernet franc',
     'olaszrizling',
     'merlot',
     'pinot noir',
     'kadarka',
     'rajnai rizling',
     'cabernet sauvignon',
     'chardonnay',
     'sárga-muskotály']
  end

  def self.genre_options
    ['vörös',
     'fehér',
     'édes',
     'rosé']
  end

  def grapes
    grapes = []

    self.grape_taggings.each do |tagging|
      grapes << tagging.grape_tag.name
    end

    grapes.map { |grape| "#{grape.downcase}" }.join(', ')
  end
end
