class WineImage < ActiveRecord::Base
  establish_connection(ENV['GMDB_DATABASE_URL'])
  self.table_name = 'wine_images'

  # attr_accessible :category, :name, :public, :wine_id, :wine_image
  belongs_to :wine

  has_attached_file :wine_image, :styles => { :large => "800x600>", :medium => "300x300>", :thumb => "100x100>" }
end
