class HomeBlock < ActiveRecord::Base
  belongs_to :post

  acts_as_list

  default_scope {  order('position ASC') }

  scope :featured_blocks, lambda { where('kind = ? OR kind = ?', 'featured_post', 'custom') }
  scope :highlighted_blocks, lambda { where(:kind => 'highlighted_post').limit(4) }

  def self.style_type_options
    ['Alapértelmezett', 'A Bűvös Szakács Archívumából', 'A Mese Habbal Archívumából', 'GM Kalauz Megrendelése', 'Tokaj Guide Megrendelése']
  end
end
