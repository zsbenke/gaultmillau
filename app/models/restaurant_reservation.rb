class RestaurantReservation < ActiveRecord::Base
  include Tokenable
  belongs_to :user
  belongs_to :restaurant

  validates :book_at, :presence => true
  validates :book_time, :presence => true
  validates_format_of :email, :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/, :message => I18n.t(:invalid)
  validates :phone, :presence => true, :if => :save_phone_to_profile?
  validates :feedback_type, :inclusion => { :in => ['email', 'phone'] }
  validates :number_of_persons, :presence => true
  validates_uniqueness_of :token

  before_create :generate_token

  default_scope { order('restaurant_reservations.created_at DESC') }

  include PgSearch
  pg_search_scope :search_by_meta, against: [:comment, :email, :phone, :book_at, :book_time, :number_of_persons],
                  :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  def save_phone_to_profile?
    if save_phone_to_profile == true || feedback_type == 'phone'
      true
    else
     false
    end
  end

  def self.number_of_persons_options
    ['1 fő', '2 fő', '3 fő', '4 fő', '5 fő', '6 fő', '10 fő', 'Több mint 10 fő']
  end

  def self.book_time_options
    ['07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30']
  end

  def self.filter_by_meta(keyword)
    if !keyword.blank?
      search_by_meta(keyword)
    else
      order('restaurant_reservations.created_at DESC')
    end
  end
end
