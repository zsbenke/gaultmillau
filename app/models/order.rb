class Order < ActiveRecord::Base
  include Tokenable
  belongs_to :user
  belongs_to :product

  validates :address, :city, :full_name, :postcode, :product_count, :presence => true
  validates :billing_name, :billing_postcode, :billing_city, :billing_address, :presence => true, :if => :billing_same_as_order?
  validates :product_count, :numericality => { :greater_than => 0, :only_integer => true }
  validates :payment_method, :inclusion => { :in => ['delivery', 'transfer'] }
  validates_format_of :email, :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/, :message => I18n.t(:invalid)
  validates_uniqueness_of :token, :payment_token

  before_create :generate_token, :generate_payment_token, :check_billing_fields, :assign_default_status

  has_paper_trail :on => [:update, :destroy]

  default_scope { order('created_at DESC') }

  include PgSearch
  pg_search_scope :search_by_meta, against: [:address, :city, :email, :full_name, :phone, :postcode, :product_count, :billing_postcode, :billing_city, :billing_address, :billing_name, :billing_phone, :payment_token], associated_against: { :product => [:name] },
                  :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :for_user, lambda { |user| where(:user_id => user.id) unless user.blank? }
  scope :by_status, lambda { |status| where(:status => status) unless status.blank? }
  scope :by_payment_method, lambda { |payment_method| where(:payment_method => payment_method) unless payment_method.blank? }

  def full_address
    "#{postcode} #{city}, #{address}"
  end

  def full_billing_address
    "#{billing_postcode} #{billing_city}, #{billing_address}"
  end

  def billing_same_as_order?
    if billing_address_same_as_order_address == true
      false
    else
      true
    end
  end

  def check_billing_fields
    if billing_address_same_as_order_address == true
      self.billing_name = nil
      self.billing_phone = nil
      self.billing_postcode = nil
      self.billing_city = nil
      self.billing_address = nil
      self.vat_number = nil
    end
  end

  def generate_payment_token
    begin
      random = SecureRandom.uuid
      payment_token = random[0..5]
    end while Order.where(:payment_token => payment_token).exists?
    self.payment_token = payment_token
  end

  def assign_default_status
    self.status = "Új rendelés"
  end

  def total
    if self.product_count > self.product.free_delivery_from_quantity
      self.product.price * self.product_count
    else
      (self.product.price * self.product_count) + self.product.delivery_price
    end
  end

  def payment_method_label
    if self.payment_method == 'delivery'
      I18n.t('helpers.label.order.payment_methods.delivery')
    elsif self.payment_method == 'transfer'
      I18n.t('helpers.label.order.payment_methods.transfer')
    end
  end

  def is_transfer?
    self.payment_method == 'transfer'
  end

  def self.transfer_info
    I18n.t('orders.messages.transfer_info').html_safe
  end

  def self.filter_by_meta(keyword)
    if !keyword.blank?
      search_by_meta(keyword)
    else
      order('orders.created_at DESC')
    end
  end

  def self.options_for_status
    ['Új rendelés', 'Kipostázott', 'Kézbesítő átvette', 'Vevő átvette', 'Lemondott']
  end

  def self.options_for_payment_method
    [[I18n.t('helpers.label.order.payment_methods.delivery'), 'delivery'], [I18n.t('helpers.label.order.payment_methods.transfer'), 'transfer']]
  end
end
