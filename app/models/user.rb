class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :posts
  has_many :comments
  has_many :orders
  has_many :newsletter_subscriptions
  has_many :favourites
  has_many :restaurant_reservations

  has_one :redeem_code

  validates :username, :presence => true
  validates :username, :uniqueness => true
  validates_format_of :username, :with => /\A[A-Za-z0-9\d_]+\z/, :message => I18n.t(:invalid)

  after_create :assign_restaurant_reservations

  has_paper_trail :on => [:destroy]

  include PgSearch
  pg_search_scope :search_by_meta, against: [:username, :email],
                  :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :admins, lambda { where('role_admin = ? OR role_editor = ?', true, true) }
  scope :users, lambda { where role_admin: [false, nil], role_editor: [false, nil],
                                    role_accountant: [false, nil], role_senior_editor: [false, nil] }
  scope :confirmed, lambda { where.not confirmed_at: nil }
  scope :redeem_code_assigned, lambda { |has_redeem_code| joins(:redeem_code).where('redeem_codes.user_id IS NOT NULL') unless has_redeem_code.blank? }

  def convert_to_admin!
    update({
      :role_admin         => true,
      :role_senior_editor => false,
      :role_editor        => false,
      :role_accountant    => false
    }, :as => :admin)
  end

  def convert_to_senior_editor!
    update({
      :role_admin         => false,
      :role_senior_editor => true,
      :role_editor        => false,
      :role_accountant    => false
    }, :as => :admin)
  end

  def convert_to_editor!
    update({
      :role_admin         => false,
      :role_senior_editor => false,
      :role_editor        => true,
      :role_accountant    => false
    }, :as => :admin)
  end

  def convert_to_accountant!
    update({
      :role_admin         => false,
      :role_senior_editor => false,
      :role_editor        => false,
      :role_accountant    => true
    }, :as => :admin)
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.filter_by_meta(keyword)
    if !keyword.blank?
      search_by_meta(keyword)
    else
      order('users.created_at DESC')
    end
  end

  def assign_restaurant_reservations
    restaurant_reservations = RestaurantReservation.where(:email => self.email, :save_to_users => true)

    restaurant_reservations.each do |restaurant_reservation|
      restaurant_reservation.user = self
      restaurant_reservation.save
    end
  end

  def admin?
    role_admin? || role_senior_editor? || role_editor?
  end

  def has_redeem_code?
    !self.redeem_code.nil?
  end
end
