class GrapeTag < GmdbConnection
  self.table_name = 'tags'

  has_many :grape_taggings, :foreign_key => 'tag_id'

  def self.by_name name
    where('lower(name) = ?', "#{name.downcase}").first unless name.blank?
  end
end
