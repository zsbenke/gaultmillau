class Post < ActiveRecord::Base
  belongs_to :user, :counter_cache => true
  has_many :comments, :dependent => :destroy
  has_many :blocks, -> { order('position ASC') }, :dependent => :destroy
  has_many :home_blocks, :dependent => :destroy
  belongs_to :attachment

  validates :title, :presence => true
  acts_as_taggable
  acts_as_url :title, :sync_url => true, :scope => :category

  has_paper_trail :on => [:update, :destroy]

  include PgSearch
  pg_search_scope :by_keyword, against: [:title, :excerpt],
                  associated_against: { :blocks => [:content] },
                  :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  scope :by_categories, lambda { |category| where(:category => category) unless category.blank? }
  scope :by_author, lambda { |author_id| where(:user_id => author_id) unless author_id.blank? }

  scope :amuva_public, lambda { where(:category => 'a-muvelt-alkoholista', :public => true) }
  scope :amuva_posts, lambda { where(:category => 'a-muvelt-alkoholista') }

  scope :bsz_public, lambda { where(:category => 'buvos-szakacs', :public => true) }
  scope :bsz_posts, lambda { where(:category => 'buvos-szakacs') }

  scope :mh_public, lambda { where(:category => 'mese-habbal', :public => true) }
  scope :mh_posts, lambda { where(:category => 'mese-habbal') }

  scope :news_public, lambda { where(:category => 'hirek', :public => true) }
  scope :news_posts, lambda { where(:category => 'hirek') }

  scope :recipes_public, lambda { where(:category => 'receptek', :public => true) }
  scope :recipes_posts, lambda { where(:category => 'receptek') }

  scope :recipes_tagged_with, lambda { |tag| tagged_with(tag) unless tag.blank? }
  scope :public, lambda { where(:public => true) }
  scope :for_main_feed, lambda { where("posts.public = TRUE AND posts.category != 'oldalak' AND posts.category != 'receptek' AND posts.category != 'hirdetesek' AND posts.category != 'beagyazott'") }
  scope :latest_recipe_with_image, lambda { |category| tagged_with(category).joins(:blocks).where('blocks.kind = ?', 'image') }
  scope :not_recipe, lambda { where('posts.category != ?', 'receptek') }

  def self.categories
    [['A Művelt Alkoholista', 'a-muvelt-alkoholista'],
     ['Bűvös Szakács', 'buvos-szakacs'],
     ['Mese Habbal', 'mese-habbal'],
     ['G&M Magazin', 'hirek'],
     ['Receptek', 'receptek'],
     ['Oldalak', 'oldalak'],
     ['Kalauz 2012', 'kalauz-2012'],
     ['Kalauz 2013', 'kalauz-2013'],
     ['Kalauz 2014', 'kalauz-2014'],
     ['Kalauz 2015', 'kalauz-2015'],
     ['Kalauz 2016', 'kalauz-2016'],
     ['Kalauz 2017', 'kalauz-2017'],
     ['Beágyazott', 'beagyazott'],
     ['Hirdetések', 'hirdetesek']]
  end

  def self.recipe_tags
    ['alapkészítmény mártás',
     'leves',
     'zöldségételek',
     'előétel és saláta',
     'hal és tenger gyümölcsei',
     'tészta',
     'négylábú',
     'szárnyas',
     'desszert',
     'egyéb']
  end

  def self.recipe_kitchens
    ['amerikai konyha',
     'angol konyha',
     'arab konyha',
     'baszk konyha',
     'belga konyha',
     'brazil konyha',
     'elzászi konyha',
     'francia konyha',
     'görög konyha',
     'indiai konyha',
     'indonéz konyha',
     'japán konyha',
     'kínai konyha',
     'koreai konyha',
     'lengyel konyha',
     'libanoni konyha',
     'magyar ízek',
     'német konyha',
     'olasz konyha',
     'osztrák konyha',
     'portugál konyha',
     'spanyol konyha',
     'szerb konyha',
     'thai konyha',
     'török konyha',
     'vietnámi konyha',
     'zsidó konyha']
  end

  def self.main_bsz_categories
    ['hozzávaló', 'technológia', 'konyhariport', 'karizmatikus étel', 'gasztrokultúra','mge']
  end

  def self.post_class_options
    [['Alapértelmezett', 'default'],
     ['Éttermek', 'restaurants'],
     ['Szállodák', 'hotels'],
     ['Borászatok', 'wineries'],
     ['Borok', 'wines'],
     ['A Művelt Alkoholista', 'amuva_posts'],
     ['Bűvös Szakács', 'bsz_posts'],
     ['Mese Habbal', 'mh_posts'],
     ['G&M Magazin', 'news'],
     ['Receptek', 'recipes']]
  end

  def self.filter_by_meta(keyword)
    if !keyword.blank?
      by_keyword(keyword)
    else
      order('posts.published_at DESC')
    end
  end

  def self.last_blog_post
    Post.where('(posts.category != ? AND posts.category != ?) AND posts.public = ?', 'hirdetesek', 'beagyazott', true).last
  end
end
