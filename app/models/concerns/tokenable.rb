module Tokenable
  extend ActiveSupport::Concern

  protected
    def generate_token
      begin
        token = SecureRandom.urlsafe_base64
      end while Order.where(:token => token).exists?
      self.token = token
    end
end
