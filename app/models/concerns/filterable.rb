module Filterable
  extend ActiveSupport::Concern

  included do
    COUNTRIES = ['HU – Magyarország', 'CZ – Csehország', 'SK – Szlovákia', 'UA – Ukrajna', 'RO – Románia', 'RS – Szerbia', 'HR – Horvátország', 'SI – Szlovénia', 'AT – Ausztria']
    scope :by_country, lambda { |country| where('country ilike ?', "%#{country}%") unless country.blank? }
  end
end