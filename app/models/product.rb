class Product < ActiveRecord::Base
  has_many :orders
  validates :name, :price, :delivery_price, :free_delivery_from_quantity, presence: true
  validates :external_url, url: { allow_blank: true }

  has_paper_trail :on => [:update, :destroy]

  default_scope { order('created_at DESC') }

  scope :available, lambda { where(:available => true) }
  scope :public, lambda { where(:available => true, :public => true) }
end
