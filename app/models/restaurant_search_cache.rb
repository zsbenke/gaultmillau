class RestaurantSearchCache < ActiveRecord::Base
  validates :name, :presence => true, :uniqueness => true

  def self.update_cache
    self.restaurants.pluck(:title).flatten.uniq.each do |cache_name|
      self.add_to_cache(cache_name)
    end
    self.restaurants.pluck(:city).flatten.uniq.each do |cache_name|
      self.add_to_cache(cache_name)
    end
    self.restaurants.pluck(:def_people_one_name).flatten.uniq.each do |cache_name|
      self.add_to_cache(cache_name)
    end
    self.restaurants.pluck(:def_people_two_name).flatten.uniq.each do |cache_name|
      self.add_to_cache(cache_name)
    end
    self.restaurants.pluck(:def_people_three_name).flatten.uniq.each do |cache_name|
      self.add_to_cache(cache_name)
    end
    self.restaurants.pluck(:tags_index).flatten.uniq.each do |cache_name|
      cache_name.split(',').each do |tag|
        self.add_to_cache(tag)
      end
    end

    self.restaurants.pluck(:country).uniq.each do |country|
      add_to_cache UnicodeUtils.downcase(country.split('–')[1].strip)
    end

    self.add_to_cache "nyitva hétfőn"
    self.add_to_cache "nyitva vasárnap"
    self.add_to_cache "hétfőn nyitva"
    self.add_to_cache "vasárnap nyitva"
    self.add_to_cache "pop helyek"
    self.add_to_cache "éttermek"
    self.add_to_cache "összes adatbázisunkban levő hely"
    self.add_to_cache "vidéki helyek"

    self.restaurants.each do |r|
      self.generate_search_cache(r)
    end

  end

  def self.update_ranks
    self.all.each do |cache|
      cache.rank = self.restaurants.by_keyword(cache.name).count
      cache.save
    end
  end

  def self.reset_cache
    self.destroy_all
    self.update_cache
  end

  def self.add_to_cache(cache_name)
    cache_name = UnicodeUtils.downcase(cache_name.strip.gsub(",", "").gsub("--", ""))
    cache = self.where(name: cache_name).first
    cache = self.create(name: cache_name) if cache.nil?
    return cache
  end

  def self.generate_search_cache(restaurant)
    tags = restaurant.tags_index.split(', ').map { |t| UnicodeUtils.downcase(t.strip) }
    tags = tags.flatten.compact
    definitions = [restaurant.def_people_one_name, restaurant.def_people_two_name, restaurant.def_people_three_name, restaurant.city, restaurant.title].flatten.compact.map { |t| UnicodeUtils.downcase(t.strip) }
    (tags + definitions).each { |keyword| self.add_to_cache(keyword) }
  end

  private
    def self.restaurants
      restaurants = Restaurant.public
    end
  end
