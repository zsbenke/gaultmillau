require 'fileutils'

class Export
  attr_accessor :posts

  def dump(category:)
    @posts ||= Post.where(category: category)
    @export_dir = Rails.root.join('tmp', 'export', category)
    @img_dir = @export_dir.join('images')

    FileUtils.remove_dir(@export_dir) if File.exist?(@export_dir)
    FileUtils.mkdir_p(@export_dir)
    FileUtils.mkdir_p(@img_dir)

    @posts.each do |post|
      post_img_dir = @img_dir.join(post.url.to_s)
      FileUtils.mkdir_p(post_img_dir)

      post_status = post.public? ? 'public' : 'private'
      post_url = self.class.url_for(post: post)
      post_export_file = @export_dir.join("#{post.url}.html")
      write_log "Started exporting post #{post.url} at #{post_url}"

      # set the post public temporary for HTML dump
      post.update_attribute(:public, true) if post_status == 'private'

      doc = Nokogiri::HTML(open(post_url))
      post_node = doc.css('article.post').first
      comment_nodes = doc.css('article.comment')

      next unless post_node.present?

      post_node.css('.short-url').first.remove
      post_node.css('.post-tags-popover-toggle').remove
      post_node.css('.post-meta-icon.comments-count').remove
      post_node.css('.related-recipes').remove

      post_node.css('img').each do |img|
        attachment_id = img[:src].match(/system\/attachments\/[0-9]+\//).to_s.split('/').last
        next unless attachment_id

        attachment = Attachment.where(id: attachment_id).first
        next unless attachment.present?

        original_image_url = attachment.attachment.url(:original)

        begin
          downloaded_image = open(original_image_url).read

          filename = "#{attachment_id}#{File.extname(attachment.attachment_file_name)}"
          File.open(post_img_dir.join(filename), 'wb') do |file|
            file.write downloaded_image
            write_log "Downloaded attachment ##{attachment.id} from #{original_image_url}"
          end
          img[:src] = "images/#{post.url}/#{filename}"
        rescue
          img[:src] = nil
          write_log "Missing attachment ##{attachment.id} at #{original_image_url}"
        end
      end

      post_node.css('.embedded-recipe').each do |recipe_node|
        recipe_url = recipe_node['data-recipe-url']
        link_to_recipe_node = doc.create_element 'a'
        link_to_recipe_node[:href] = "../receptek/#{recipe_url}.html"
        link_to_recipe_node.content = "#{recipe_url}"
        recipe_node.inner_html = link_to_recipe_node
        write_log "Replaced recipe with slug '#{recipe_url}'"
      end

      export_html = """
        <!DOCTYPE html>
        <html lang=\"hu\">
          <head>
            <title>Post #{post.url}</title>
            <meta charset=\"utf-8\" />
          </head>
          <body style=\"font-family: Helvetica, Arial, sans-serif;\" class=\"#{post_status}\">
            #{post_node}
            <div id=\"comments\">
              #{comment_nodes}
            </div>
          </body>
        </html>
      """

      File.write(post_export_file, export_html)

      # switch back private posts as private
      post.update_attribute(:public, false) if post_status == 'private'

      write_log "Finished exporting post #{post.url} at #{post_url}"
    end
  end

  class << self
    def url_for(post:)
      case post.category
      when 'a-muvelt-alkoholista' then Rails.application.routes.url_helpers.amuva_post_url(post.url, host: default_host)
      when 'buvos-szakacs' then Rails.application.routes.url_helpers.bsz_post_url(post.url, host: default_host)
      when 'mese-habbal' then Rails.application.routes.url_helpers.mh_post_url(post.url, host: default_host)
      when 'receptek' then Rails.application.routes.url_helpers.recipe_url(post.url, host: default_host)
      else nil
      end
    end

    def default_host
      Rails.configuration.action_controller.default_url_options[:host]
    end
  end

  private
    def write_log(msg)
      import_log_file = @export_dir.join('export.log')
      File.open(import_log_file, 'a+') { |file| file.write(Time.now.strftime("%Y-%m-%d %H:%M:%S - ") + "#{msg}\n") }
      puts msg
    end
end
