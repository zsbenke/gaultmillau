class Feedback < ActiveRecord::Base
  validates :content, :presence => true
  validates_format_of :address, :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/, :message => I18n.t(:invalid)
  validates :captcha, :inclusion => { :in => ['kevés lisztet tejfölbe keverve, majd ételhez hozzáadva, sűrítés', 'alacsony hőmérsékleten forrpont alatti főzés', 'folyadék aromatizálása valamilyen ízesítőanyag beáztatásával'] }
end
