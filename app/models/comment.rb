class Comment < ActiveRecord::Base
  belongs_to :user, :counter_cache => true
  belongs_to :post, :counter_cache => true, :touch => true

  validates :body, :presence => true

  has_paper_trail :on => [:update, :destroy]

  include PgSearch
  pg_search_scope :search_by_meta, against: [:body],
                  associated_against: { :user => [:username, :email], :post => [:title]},
                  :using => { :tsearch => {:dictionary => 'hungarian', :prefix => true} }

  default_scope { order('created_at DESC') }

  scope :latest_comments, lambda { joins(:post).where('posts.public = true').limit(8).order('created_at DESC') }
  scope :amuva_comments, lambda { joins(:post).where('posts.category = ?', "a-muvelt-alkoholista").limit(40) }
  scope :bsz_comments, lambda { joins(:post).where('posts.category = ?', "buvos-szakacs").limit(40) }
  scope :mh_comments, lambda { joins(:post).where('posts.category = ?', "mese-habbal").limit(40) }
  scope :news_comments, lambda { joins(:post).where('posts.category = ?', "hirek").limit(40) }

  def self.filter_by_meta(keyword)
    if !keyword.blank?
      search_by_meta(keyword)
    else
      order('comments.created_at DESC')
    end
  end
end
