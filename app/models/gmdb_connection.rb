class GmdbConnection < ActiveRecord::Base
  self.abstract_class = true
  establish_connection(ENV['GMDB_DATABASE_URL'])
end