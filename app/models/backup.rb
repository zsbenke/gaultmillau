class Backup
  attr_accessor :name, :path, :size, :created_at

  def initialize(options = {})
    @name = options[:name]
    @path = options[:path]
    @size = options[:size]
    @created_at = options[:created_at]
  end

  def self.all
    Dir.entries(self.backup_path).select { |f| f.match /#{database_name}/ }.map { |f| self.find(f) }.sort_by(&:created_at).reverse
  end

  def self.all_restaurant_backup
    Dir.entries(self.backup_path).select { |f| f.match /#{restaurant_database_name}/ }.map { |f| self.find(f) }.sort_by(&:created_at).reverse
  end

  def self.find(basename)
    file = File.new("#{self.backup_path}/#{basename}", 'r')
    file_stat = File::Stat.new file.path
    self.new(
      name: File.basename(file.path),
      path: file.path,
      size: file_stat.size,
      created_at: file_stat.ctime
    )
  end

  def self.backup_path
    if Rails.configuration.respond_to?(:backup_path)
      Rails.configuration.backup_path
    else
      File.expand_path('~/db_backup/')
    end
  end

  def self.database_name
    ActiveRecord::Base.connection.current_database
  end

  def self.restaurant_database_name
    ActiveRecord::Base.connection.current_database.gsub('gaultmillau', 'gmdb')
  end

  def self.create
    database_name = self.database_name
    backup_dir = Backup.backup_path
    dump_file = "#{database_name}_#{Time.now.strftime("%Y_%m_%d_%H_%M")}.sql"
    gzip_file = dump_file.gsub('.sql', '')


    # create backup directory
    if !File.directory? backup_dir
      `mkdir #{backup_dir}`
    end

    # generate new DB dump and compress it
    `pg_dump #{database_name} > ~/#{dump_file} && gzip ~/#{dump_file} && mv ~/#{dump_file}.gz #{backup_dir}/#{gzip_file}.gz`
    Rails.logger.info "Created new backup: #{dump_file}"

    # remove stale backups (stale rules defined in the stale? method)
    Backup.all.each do |backup|
      backup.destroy if backup.stale?
    end

    # copy backups to an offsite backup server
    if ENV['BACKUP_SERVER_ADDRESS'].present? and
       ENV['BACKUP_SERVER_USERNAME'].present? and
       ENV['BACKUP_SERVER_FOLDER_PATH'].present?
       sync_server_path = "#{ENV['BACKUP_SERVER_USERNAME']}@#{ENV['BACKUP_SERVER_ADDRESS']}:#{ENV['BACKUP_SERVER_FOLDER_PATH']}"
       output = `rsync -avz --delete #{backup_path}/ #{sync_server_path}`
       Rails.logger.info "Synced backups to offsite server (#{sync_server_path}) with output: #{output}"
    end
  end

  def self.restore_restaurant_database
    backup = all_restaurant_backup.first
    sql_name = backup.name.gsub('.gz', '')

    `cp #{backup.path} ~/`
    `gunzip ~/#{backup.name}`
    ActiveRecord::Base.connection.execute("SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '#{restaurant_database_name}' AND pid <> pg_backend_pid();")
    `dropdb #{restaurant_database_name}; createdb #{restaurant_database_name}; psql #{restaurant_database_name} < ~/#{sql_name}`
    `rm ~/#{sql_name}`
  end

  def stale?
    created_at < 30.days.ago
  end

  def destroy
    File.delete(path)
  end
end
