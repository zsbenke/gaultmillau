class Block < ActiveRecord::Base
  belongs_to :post, :touch => true
  belongs_to :attachment
  acts_as_list scope: :post

  has_paper_trail :on => [:update, :destroy]

  scope :parent_posts, lambda { |post_id| where(:content => "#{post_id}") unless post_id.blank? }
  scope :attachment_blocks, lambda { where :kind => 'image' }

  def self.text_kind_options
    [[I18n.t('admin.posts.block_options.kind.text'), 'text'],
     [I18n.t('admin.posts.block_options.kind.title'), 'title'],
     [I18n.t('admin.posts.block_options.kind.bordered_text'), 'bordered-text']]
  end

  def self.kind_options
    self.text_kind_options + [[I18n.t('admin.posts.block_options.kind.image'), 'image'],
                              [I18n.t('admin.posts.block_options.kind.video'), 'video'],
                              [I18n.t('admin.posts.block_options.kind.recipe'), 'recipe'],
                              [I18n.t('admin.posts.block_options.kind.post'), 'post']]
  end

  def self.image_class_options
    [['Alap', 'default'],
     ['Középre zárt', 'centered'],
     ['Teljes', 'full-width']]
  end
end
