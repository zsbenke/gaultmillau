class GrapeTagging < GmdbConnection
  self.table_name = 'taggings'

  belongs_to :wine, :foreign_key => 'taggable_id'
  belongs_to :grape_tag, :foreign_key => 'tag_id'
end
