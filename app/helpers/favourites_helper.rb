module FavouritesHelper
  def faved_by user, record_id
    favourite = Favourite.where(:user_id => user.id, :record_id => record_id).first.present?
  end

  def get_fav_for user, record_id
    Favourite.where(:user_id => user.id, :record_id => record_id).first
  end

  def get_record_title_for_fav favourite
    case favourite.kind
      when 'restaurant'
        Restaurant.find(favourite.record_id).title
      when 'hotel'
        Hotel.find(favourite.record_id).title
      when 'winery'
        Winery.find(favourite.record_id).title
    end
  end

  def get_record_path_for_fav favourite
    case favourite.kind
      when 'restaurant'
        restaurant_path Restaurant.find(favourite.record_id)
      when 'hotel'
        hotel_path Hotel.find(favourite.record_id)
      when 'winery'
        winery_path Winery.find(favourite.record_id)
    end
  end

  def record_exists_for_fav? favourite
    case favourite.kind
      when 'restaurant'
        Restaurant.where(:public => true, :year => CONFIG_GM_YEAR, :id => favourite.record_id).exists?
      when 'hotel'
        Hotel.where(:public => true, :year => CONFIG_GM_YEAR, :id => favourite.record_id).exists?
      when 'winery'
        Winery.where(:public => true, :year => CONFIG_GM_YEAR, :id => favourite.record_id).exists?
    end
  end
end
