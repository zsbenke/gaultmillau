module FeedbacksHelper
  def captcha_questions
    ['Mi az a habarás?',
     'Mi az a posírozás?',
     'Mi az infúzionálás?']
  end

  def captcha_anwsers
    [['kevés lisztet tejfölbe keverve, majd ételhez hozzáadva, sűrítés',
      'a serpenyő alján karamellizálódott fehérje felvakarása',
      'tojásfehérje felverése habbá'],
     ['alacsony hőmérsékleten forrpont alatti főzés',
      'saláta alapos centrifugázása, folyadékmentesítése',
      'lágytojáskészítés'],
     ['folyadék aromatizálása valamilyen ízesítőanyag beáztatásával',
      'folyadék szárnyasokba juttatása',
      'a marinálás szinonim fogalma']]
  end
end
