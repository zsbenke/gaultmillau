module PostsHelper
  def find_all_authors
    users = User.admins
    authors = []
    users.each do |user|
      authors << [user.username, user.id]
    end

    return authors
  end

  def current_post_category(params)
    Post.categories.select do |s|
      if s.include? params[:category]
        return s[0].to_s
      end
    end
  end

  def get_category_for(post)
    Post.categories.select do |s|
      if s.include? post.category
        return s[0].to_s
      end
    end
  end

  def block_text_for(block)
    case block.kind
    when 'picture' then 'Kép'
    when 'title' then 'Alcím'
    when 'text' then 'Szöveg'
    when 'module' then 'Modul'
    end
  end

  def find_all_recipe
    recipes = Post.where(:category => 'receptek').order('unaccent(title) ASC')
    recipes_options = []
    recipes.each do |recipe|
      recipes_options << [recipe.title, recipe.id]
    end

    return recipes_options
  end

  def find_all_post block
    posts = Post.not_recipe.order('published_at DESC')
    posts_options = []
    posts.each do |post|
      if post != block.post
        posts_options << ["#{post.title} &ndash; #{get_category_for(post)}".html_safe, post.id]
      end
    end

    return posts_options
  end

  def link_to_recipe(block)
    recipe = Post.find_by_id(block.content)
    if recipe
      link_to "#{recipe.title} recept", edit_admin_post_path(recipe)
    else
      content_tag(:p, t('admin.posts.messages.notices.blocks.post_not_found_inline'), :class => 'info-panel error')
    end
  end

  def link_to_post(block)
    post = Post.find_by_id(block.content)
    if post
      link_to "&bdquo;#{post.title}&rdquo; bejegyzés".html_safe, edit_admin_post_path(post)
    else
      content_tag(:p, t('admin.posts.messages.notices.blocks.post_not_found_inline'), :class => 'info-panel error')
    end
  end

  def find_all_restaurants
    restaurants = Restaurant.public
    restaurants_options = []
    restaurants.each do |restaurant|
      restaurants_options << ["#{restaurant.title} - #{restaurant.year}", restaurant.id]
    end

    return restaurants_options
  end

  def link_to_restaurant(block)
    restaurant = block.restaurant
    restaurant_url = ENV['GMDB_URL']
    if restaurant
      link_to "#{restaurant.title} - #{restaurant.year}", "#{restaurant_url}/ettermek/#{restaurant.id}/edit"
    else
      t('admin.posts.messages.notices.blocks.post_not_found_inline')
    end
  end

  def find_post_category post
    if Post.categories.flatten.include? post.category
      Post.categories.select { |c| c if c.include?(post.category) }[0][0]
    end
  end

  def show_category_for current_user, category
    if (!current_user.role_admin && category == 'hirdetesek') ||
       (!current_user.role_admin && category == 'beagyazott')
      "hidden"
    end
  end

  def current_post_category_link params
    if !params[:author].blank? || !params[:tag].blank?
      link_to current_post_category(params), admin_posts_path(:category => params[:category])
    else
      current_post_category(params)
    end
  end

  def filtered_posts_labels params
    if !params[:author].blank?
      " / ".html_safe + content_tag('span', "#{User.find(params[:author]).username}".html_safe, :class => 'breadcrumb-tag')
    elsif !params[:tag].blank?
      " / ".html_safe + content_tag('span', "#{params[:tag]}".html_safe, :class => 'breadcrumb-tag')
    end
  end

  def render_embedded_page page_url
    post = Post.public.find_by_url(page_url)

    if !post.nil?
      post.blocks.each do |block|
        concat(render(:partial => 'blocks/block', :locals => { :post => post, :block => block }))
      end
    end
  end

  def format_post_date post
    post.public ? l(post.published_at, :format => :short) : content_tag(:span, l(post.created_at, :format => :short), :class => 'not-published')
  end

  def category_tile_width category
    if category == 'a-muvelt-alkoholista' ||
       category == 'buvos-szakacs' ||
       category == 'mese-habbal' ||
       category == 'hirek'
      "six"
    else
      "three"
    end
  end
end
