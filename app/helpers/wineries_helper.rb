module WineriesHelper
  def winery_image_url winery_image, size = 'large'
    "#{ENV['GMDB_RACKSPACE_HOST']}/winery_images/#{winery_image.id}/#{size}/#{winery_image.winery_image_file_name}"
  end

  def winery_params_blank?
    params[:search].blank? &&
    params[:rating].blank? &&
    params[:region].blank? &&
    params[:country].blank? &&
    params[:wine_rating].blank? &&
    params[:wine_grape].blank? &&
    params[:wine_genre].blank?
  end

  def public_winery? winery
    winery && winery.public?
  end
end
