module ApplicationHelper
  class FoundationLinkRenderer < ::WillPaginate::ActionView::LinkRenderer
    protected
      def html_container html
        tag(:ul, html, container_attributes)
      end

      def page_number page
        tag :li, link(page, page, :rel => rel_value(page)), :class => ('current' if page == current_page)
      end

      def gap
        tag :li, link(super, '#'), :class => 'unavailable'
      end

      def previous_or_next_page page, text, classname
        tag :li, link(text, page || '#'), :class => [classname[0..3], classname, ('unavailable' unless page)].join(' ')
      end
  end

  def page_navigation_links pages, remote = false
    if remote == true
      will_paginate(pages, :remote => true, :class => 'pagination horizontal', :inner_window => 2, :outer_window => 0, :renderer => FoundationLinkRenderer, :previous_label => t('will_paginate.prev_link').html_safe, :next_label => t('will_paginate.next_link').html_safe)
    else
      will_paginate(pages, :class => 'pagination horizontal', :inner_window => 2, :outer_window => 0, :renderer => FoundationLinkRenderer, :previous_label => t('will_paginate.prev_link').html_safe, :next_label => t('will_paginate.next_link').html_safe)
    end
  end

  def flash_class level
    case level
      when :notice then "notice"
      when :success then "success"
      when :error then "error"
      when :alert then "alert"
      when :email then "email"
      when :success_modal then "success-modal"
      when :error_modal then "error-modal"
    end
  end

  def spacer_tag width
    content_tag :div, :class => 'row' do
      content_tag :div, :class => "#{width} columns" do
        content_tag :hr
      end
    end
  end

  def field_has_error? resource, field
    resource.errors[field].any? ? 'error' : ''
  end

  def field_errors resource, field
    if resource.errors[field].any?
      content_tag :small do
        resource.errors[field].each do |message|
          concat content_tag(:span, message)
        end
      end
    end
  end

  def site_title
    if ENV['GM_VERSION']
      "#{CONFIG_SITE_NAME} (#{ENV['GM_VERSION']})"
    else
      CONFIG_SITE_NAME
    end
  end

  def excerpt_for_text(text, length = 400)
    raw(truncate(strip_tags(text), :length => length, :omission => "…"))
  end

  def clear_excerpt_field(excerpt_field)
    raw(strip_tags(excerpt_field))
  end

  def format_count(number)
    if number == 0
      t('shared.counts.none')
    else
      t('shared.counts.postfix', :number => number_with_delimiter(number))
    end
  end

  def markdown(text)
    options = { :autolink => true, :space_after_headers => true, :no_intra_emphasis => false }
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(:hard_wrap => true), options)
    markdown.render(text).html_safe
  end

  def check_empty_entry_data(entry_data, postfix = false)
    if !entry_data.blank? && postfix == false
      entry_data
    elsif !entry_data.blank? && postfix != false
      "#{entry_data} #{postfix}"
    else
      "--"
    end
  end

  def check_link_for link
    if link.blank?
      '--'
    else
      begin
        url = URI.parse(link)
        if !url.scheme
          markdown "http://#{link}"
        else %w{http https}.include?(url.scheme)
          markdown link
        end
      rescue
        '--'
      end
    end
  end

  def check_mail_link_for link
    if link.blank?
      '--'
    else
      markdown link
    end
  end

  def has_thumbnail_image? object
    if object.attachment
      object.attachment
    else
      image_block = object.blocks.where(:kind => 'image').first
      if !image_block.nil?
        if !image_block.attachment.nil?
          image_block.attachment
        end
      end
    end
  end

  def thumbnail_image_for object, size = :medium
    block = has_thumbnail_image? object
    if block
      processed_attachment_url block, size
    end
  end

  def thumbnail_image_average_color_for object
    block = has_thumbnail_image? object
    if block
      block.average_color
    end
  end

  def get_category_path_for post
    if post.category == 'buvos-szakacs'
      bsz_posts_path
    elsif post.category == 'a-muvelt-alkoholista'
      amuva_posts_path
    elsif post.category == 'mese-habbal'
      mh_posts_path
    elsif post.category == 'receptek'
      recipes_path
    elsif post.category == 'hirek'
      news_index_path
    end
  end

  def get_post_path_for post
    if post.category == 'buvos-szakacs'
      bsz_post_path(post.url)
    elsif post.category == 'a-muvelt-alkoholista'
      amuva_post_path(post.url)
    elsif post.category == 'mese-habbal'
      mh_post_path(post.url)
    elsif post.category == 'receptek'
      recipe_path(post.url)
    elsif post.category == 'hirek'
      news_path(post.url)
    elsif post.category == 'kalauz-2012'
      guide2012_path(post.url)
    elsif post.category == 'kalauz-2013'
      guide2013_path(post.url)
    elsif post.category == 'kalauz-2014'
      guide2014_path(post.url)
    elsif post.category == 'kalauz-2015'
      guide2015_path(post.url)
    elsif post.category == 'kalauz-2016'
      guide2016_path(post.url)
    elsif post.category == 'kalauz-2017'
      guide2017_path(post.url)
    elsif post.category == 'oldalak'
      page_path(post.url)
    end
  end

  def get_post_url_for post
    if post.category == 'buvos-szakacs'
      bsz_post_url(post.url)
    elsif post.category == 'a-muvelt-alkoholista'
      amuva_post_url(post.url)
    elsif post.category == 'mese-habbal'
      mh_post_url(post.url)
    elsif post.category == 'receptek'
      recipe_url(post.url)
    elsif post.category == 'hirek'
      news_url(post.url)
    elsif post.category == 'kalauz-2014'
      guide2014_url(post.url)
    elsif post.category == 'kalauz-2015'
      guide2015_url(post.url)
    elsif post.category == 'oldalak'
      page_url(post.url)
    end
  end

  def articalize word, word_case = "lowercase"
    return word if word.downcase.match(/^a /) || word.downcase.match(/^az /)
    consonants = %w{2 3 4 6 7 8 9 q w r t z p s d f g h j k l y x c v b n m}
    vowels = %w{1 5 ö ü ó e u i o ő ú a é á ű í}
    first_letter = word[0]
    if consonants.include? first_letter.downcase
      if word_case == "lowercase"
        return t('shared.article.lowercase_consonant', :word => word)
      elsif word_case == "uppercase"
        return t('shared.article.uppercase_consonant', :word => word)
      end
    elsif vowels.include? first_letter.downcase
      if word_case == "lowercase"
        return t('shared.article.lowercase_vowel', :word => word)
      elsif word_case == "uppercase"
        return t('shared.article.uppercase_vowel', :word => word)
      end
    end
  end

  def quote_before
    t('shared.messages.quotes.before').html_safe
  end

  def quote_after
    t('shared.messages.quotes.after').html_safe
  end

  def get_page_title page_url
    page = Post.find_by_url page_url
    page.title if page
  end

  def has_map? record
    !record.latitude.blank? && !record.longitude.blank? && !record.zoom.blank?
  end

  def yield_viewport
    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])
    if (user_agent.mobile? && user_agent.platform != 'iPad')
      "width=device-width, minimum-scale=1.0, initial-scale=1.0, maximum-scale=1, user-scalable=no"
    else
      "width=device-width, minimum-scale=1.0, initial-scale=1.0"
    end
  end

  def render_feature_icon feature, feature_label, feature_image, row_width = 'six'
    content_tag :div, :class => "feature-icon #{row_width} columns end #{feature ? 'on' : 'off'}", 'style' => "background-image: url(/assets/feature-icons/#{feature_image}.png);" do
      feature_label
    end
  end

  def is_email?(email)
    return email.match(/\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/)
  end

  def boolean_select_options
    [[t('shared.messages.boolean_true'), 'true'], [t('shared.messages.boolean_false'), 'false']]
  end

  def link_to_root_path css_class
    link_to(t('shared.actions.back_to_home').html_safe, root_path, :class => css_class)
  end

  def record_url id, kind
    case kind
      when 'restaurant' then restaurant_url(id)
      when 'hotel' then hotel_url(id)
      when 'winery' then winery_url(id)
    end
  end

  def google_maps_api_key
    "AIzaSyDZYpokLsp2fh8lR2DH1a5myl83RQvla3Q"
  end
end
