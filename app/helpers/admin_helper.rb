module AdminHelper
  def position_in_alphabet(letter)
    alphabet = %w(a A á Á b B c C d D e E é É f F g G h H i I í Í j J k K l L m M n N o O ó Ó ö Ö ő Ő p P q Q r R s S t T u U ú Ú ü Ü ű Ű v V w W x X y Y z Z)
    return alphabet.index(letter).nil? ? 0 : alphabet.index(letter)
  end
end
