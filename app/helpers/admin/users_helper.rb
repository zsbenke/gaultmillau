module Admin::UsersHelper
  def user_role(user)
    if user.role_admin
      t('activerecord.attributes.user.role_admin')
    elsif user.role_senior_editor
      t('activerecord.attributes.user.role_senior_editor')
    elsif user.role_editor
      t('activerecord.attributes.user.role_editor')
    elsif user.role_accountant
      t('activerecord.attributes.user.role_accountant')
    else
      "--"
    end
  end

  def all_users_count users
    format_count(users.count)
  end

  def user_exists? user
    User.exists? user
  end

  def username_for user
    user_exists?(user) ? user.username : t('shared.messages.destroyed_user')
  end
end
