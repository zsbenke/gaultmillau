module Admin::AttachmentsHelper
  def current_attachment_category(params)
    Attachment.categories.select do |s|
      if s.include? params[:category]
        return s[0].to_s
      end
    end
  end

  def current_attachment_category_count(params)
    format_count(Attachment.by_categories(params[:category]).size)
  end

  def currently_processing_attachments?
    job = Delayed::Job.last

    if job && job.payload_object.class == DelayedPaperclip::Jobs::DelayedJob
      true
    else
      false
    end
  end

  def processed_attachment_url attachment, size = :small
    if !attachment.nil?
      if attachment.attachment.processing?
        "#{ENV['RACKSPACE_HOST']}/attachments/#{attachment.id}/original/#{attachment.attachment_file_name}"
      else
        attachment.attachment.url(size)
      end
    end
  end
end
