module Admin::OrdersHelper
  def all_orders_count orders
    format_count(orders.count)
  end

  def css_class_for_order_status order
    case order.status
      when 'Új rendelés' then 'order-status new'
      when 'Kipostázott' then 'order-status mailed'
      when 'Kézbesítő átvette' then 'order-status received'
      when 'Vevő átvette' then 'order-status buyer-received'
      when 'Lemondott' then 'order-status resigned'
    end
  end

  def style_for_new_orders
    'hidden' if new_orders_count == 0
  end

  def new_orders_count
    Order.by_status('Új rendelés').count
  end
end
