module Admin::RestaurantReservationsHelper
  def all_restaurant_reservations_count restaurant_reservations
    format_count(restaurant_reservations.count)
  end
end
