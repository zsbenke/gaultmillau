module Admin::HomeBlocksHelper
  def find_all_public_posts
    posts = Post.public.where.not(category: 'receptek').order('posts.published_at DESC')
    posts_options = []
    posts.each do |post|
      posts_options << ["#{post.title} - #{get_category_for post}", post.id]
    end

    return posts_options
  end

  def home_block_category home_block
    if !home_block.post.nil?
      if home_block.post.category == 'hirdetesek' ||
         home_block.style_type == 'Alapértelmezett'
          home_block.post.category
        else
          if !home_block.style_type.blank?
            home_block.style_type.parameterize
          end
        end
    else
      home_block.title
    end
  end
end
