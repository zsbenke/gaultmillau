module Admin::CommentsHelper
  def number_for_comments(comments_count)
    if comments_count > 0
      comments_count
    else
      '&nbsp;'
    end
  end

  def all_comments_count comments
    format_count(comments.count)
  end

  def highlight_admin_comment comment
    if comment.user
      if comment.user.role_admin || comment.user.role_editor
        "highlighted"
      end
    end
  end

  def style_for_unread_comments
    'hidden' if current_user.unread_comments == 0
  end

  def render_admin_comment_badge comment
    if comment.user
      if comment.user.role_admin || comment.user.role_editor
        content_tag :span, '', :class => 'admin-comment-badge'
      end
    end
  end
end
