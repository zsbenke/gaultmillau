module Admin::ProductsHelper
  def all_products_count products
    format_count(products.count)
  end
end
