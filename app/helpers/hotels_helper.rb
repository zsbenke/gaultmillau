module HotelsHelper
  def hotel_image_url hotel_image, size = 'large'
    "#{ENV['GMDB_RACKSPACE_HOST']}/hotel_images/#{hotel_image.id}/#{size}/#{hotel_image.hotel_image_file_name}"
  end

  def hotel_params_blank?
    params[:search].blank? &&
    params[:rating].blank? &&
    params[:country].blank?
  end
end
