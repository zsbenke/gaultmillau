module ShortUrlsHelper
  def get_short_url_for_post post
    CONFIG_SHORT_URL_HOST + short_url_path(post.id)
  end
end
