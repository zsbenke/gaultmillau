module RestaurantsHelper
  def restaurant_image_url(image, version)
    ENV['GMDB_RACKSPACE_HOST'] + '/restaurant_images/' + image.id.to_s + '/' + version + '/' + image.name
  end

  def has_map_for_this_restaurant?(restaurant)
    !restaurant.latitude.blank? && !restaurant.longitude.blank? && !restaurant.zoom.blank?
  end

  def restaurant_params_blank?
    params[:search].blank? &&
    params[:country].blank? &&
    params[:rating].blank? &&
    params[:tag].blank? &&
    params[:open_on_monday].blank? &&
    params[:open_on_sunday].blank?
  end
end
