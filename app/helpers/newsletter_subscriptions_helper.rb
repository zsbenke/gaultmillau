module NewsletterSubscriptionsHelper
  def has_newsletter_subscription?
    current_user.newsletter_subscription
  end
end
