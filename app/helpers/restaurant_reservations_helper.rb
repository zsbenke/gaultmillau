module RestaurantReservationsHelper
  def restaurants_for_reservation_select
    if user_signed_in?
      restaurant_favs = current_user.favourites.where :kind => "restaurant"
    else
      restaurant_favs = []
    end

    restaurants = Restaurant.cached_public

    filtered_restaurants = []
    restaurant_ids = []

    restaurant_favs.each do |favourite|
      restaurant = Restaurant.where(:public => true, :year => CONFIG_GM_YEAR, :id => favourite.record_id)
      if restaurant.exists?
        if restaurant.last.email != ''
          restaurant_ids << restaurant.last.id
          filtered_restaurants << ["♥ #{restaurant.last.title} – #{restaurant.last.city}", restaurant.last.id]
        end
      end
    end

    restaurants.each do |restaurant|
      if !restaurant_ids.include? restaurant.id
        if restaurant.email != ''
          filtered_restaurants << ["#{restaurant.title} – #{restaurant.city}", restaurant.id]
        end
      end
    end

    return filtered_restaurants
  end

  def restaurant_exists_for_restaurant_reservation? restaurant_reservation
    !restaurant_reservation.restaurant.nil? && restaurant_reservation.restaurant.year == CONFIG_GM_YEAR && restaurant_reservation.restaurant.public
  end
end
