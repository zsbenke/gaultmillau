module BszCommentsHelper
  def has_comments_for? post
    post.comments.count != 0
  end
end
