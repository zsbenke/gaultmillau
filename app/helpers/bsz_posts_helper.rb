module BszPostsHelper
  def render_bsz_footer
    render :partial => 'bsz_posts/footer'
  end

  def css_class_for_bsz_tag tag
    case tag
      when 'hozzávaló' then 'hozzavalo'
      when 'technológia' then 'technologia'
      when 'konyhariport' then 'konyhariport'
      when 'karizmatikus étel' then 'karizmatikus-etel'
      when 'gasztrokultúra' then 'gasztrokultura'
      when 'mge' then 'mge'
      else 'bsz-tag'
    end
  end
end
