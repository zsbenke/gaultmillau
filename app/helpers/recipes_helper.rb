module RecipesHelper
  def featured_recipes_for category
    Post.recipes_public.recipes_tagged_with(category).limit(8).shuffle
  end

  def filter_recipes_category_title param
    if Post.recipe_tags.flatten.include? param
      Post.recipe_tags.select { |c| c if c.include?(param) }[0]
    else
      param
    end
  end

  def find_parent_posts post
    blocks = Block.parent_posts(post.id)
    posts = []

    blocks.each do |block|
      posts << block.post
    end

    posts
  end

  def css_class_for_recipe_category category
    case category
    when 'alapkészítmény mártás' then 'alapkeszitmeny-martas'
    when 'leves' then 'leves'
    when 'zöldségételek' then 'zoldsegetelek'
    when 'előétel és saláta' then 'eloetel-es-salata'
    when 'hal és tenger gyümölcsei' then 'hal-es-tenger-gyumolcsei'
    when 'tészta' then 'teszta'
    when 'négylábú' then 'negylabu'
    when 'szárnyas' then 'szarnyas'
    when 'desszert' then 'desszert'
    when 'egyéb' then 'egyeb'
    end
  end

  def count_for_recipe_category category
    return Post.recipes_tagged_with(category).count
  end
end
