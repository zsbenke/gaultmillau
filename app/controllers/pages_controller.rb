class PagesController < ApplicationController
  include ApplicationHelper

  def show
    if (user_signed_in? && current_user.role_admin) ||
       (user_signed_in? && current_user.role_editor) ||
       (user_signed_in? && current_user.role_senior_editor)
      @page = Post.by_categories('oldalak').find_by_url(params[:id])
    else
      @page = Post.public.by_categories('oldalak').find_by_url(params[:id])

      respond_to do |format|
        format.html
      end
    end
  end
end
