class NewsCommentsController < ApplicationController
  include Concerns::Statusable
  include Concerns::Commentable

  def index
    respond_to do |format|
      format.atom { @comments = Comment.bsz_comments.order('created_at DESC') }
    end
  end

  def create
    @news = Post.find(params[:news_id])
    return if @post.not_commentable?
    @comment = @news.comments.new(comment_params)
    @comment.user = current_user

    respond_to do |format|
      if @comment.save
        format.js { update_unread_comments_for_admins }
      else
        format.js
      end
    end
  end
end
