class HomeController < ApplicationController
  def index
    @main_post_home_block = HomeBlock.find_by_kind('main_post')
    @main_post = Post.find(@main_post_home_block.post.id)
    @featured_blocks = HomeBlock.includes(:post).featured_blocks
  end

  def feed
    @posts = Post.for_main_feed.limit(40).order('published_at DESC')
    respond_to do |format|
      format.rss { render rss: @posts }
    end
  end

  def updates
    respond_to do |format|
      format.js
    end
  end

  def current_user_meta
    respond_to do |format|
      format.js
    end
  end
end
