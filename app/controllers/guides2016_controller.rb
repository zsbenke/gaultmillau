class Guides2016Controller < ApplicationController
  include ApplicationHelper

  def index
  end

  def show
    if (user_signed_in? && current_user.role_admin) ||
       (user_signed_in? && current_user.role_editor)
      @guide = Post.by_categories('kalauz-2016').find_by_url(params[:id])
    else
      @guide = Post.public.by_categories('kalauz-2016').find_by_url(params[:id])
      respond_to do |format|
        format.html
      end
    end
  end
end
