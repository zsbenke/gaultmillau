class AmuvaPostsController < ApplicationController
  def index
    if params[:tag]
      @posts = Post.includes(:user, :attachment).amuva_public().tagged_with(params[:tag]).order('published_at DESC').paginate(:per_page => 12, :page => params[:page])
    else
      @posts = Post.includes(:user, :attachment).amuva_public().order('published_at DESC').paginate(:per_page => 12, :page => params[:page])
    end

    respond_to do |format|
      format.html
      format.json { render json: @posts }
      format.rss { render rss: @posts }
    end
  end

  def show
    if (user_signed_in? && current_user.role_admin) ||
       (user_signed_in? && current_user.role_editor) ||
       (user_signed_in? && current_user.role_senior_editor)
      @post = Post.amuva_posts.find_by_url(params[:id])
    else
      @post = Post.amuva_public.find_by_url(params[:id])
    end
  end
end
