class WineriesController < ApplicationController
  def index
    session[:last_wineries_filter] = params
    search = params[:search].strip unless params[:search].nil?
    @wineries = Winery.public.by_keyword(search).
      by_country(params[:country]).
      by_region(params[:region]).
      by_rating(params[:rating]).
      by_wines_rating(params[:wine_rating]).
      by_wines_grape(params[:wine_grape]).
      by_wines_genre(params[:wine_genre]).uniq
  end

  def show
    @winery = Winery.find(params[:id])

    if !@winery.public || @winery.year != CONFIG_GM_YEAR
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def best_of_map
    search = params[:search].strip unless params[:search].nil?
    @wineries = Winery.public.by_keyword(search).by_region(params[:region]).by_rating(['best_of'])
  end
end
