class BszCommentsController < ApplicationController
  include Concerns::Statusable
  include Concerns::Commentable

  def index
    respond_to do |format|
      @comments = Comment.bsz_comments.order('created_at DESC')
      format.rss { render rss: @comments }
    end
  end

  def create
    @post = Post.find(params[:bsz_post_id])
    return if @post.not_commentable?
    @comment = @post.comments.new(comment_params)
    @comment.user = current_user

    respond_to do |format|
      if @comment.save
        format.js { update_unread_comments_for_admins }
      else
        format.js
      end
    end
  end
end
