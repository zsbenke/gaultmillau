class HotelsController < ApplicationController
  def index
    session[:last_hotels_filter] = params
    search = params[:search].strip unless params[:search].nil?
    @hotels = Hotel.public.by_keyword(search).
      by_country(params[:country]).
      by_rating(params[:rating]).
      reorder(" record_kind='Ajánlott helyek', record_kind='Tesztelt szállodák', hotels.position ASC")
  end

  def show
    @hotel = Hotel.find(params[:id])

    if !@hotel.public || @hotel.year != CONFIG_GM_YEAR
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def best_of_map
    search = params[:search].strip unless params[:search].nil?
    @hotels = Hotel.public.by_keyword(search).by_rating(['5'])
  end
end
