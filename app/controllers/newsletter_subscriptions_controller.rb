class NewsletterSubscriptionsController < ApplicationController
  include ApplicationHelper

  def index
    redirect_to new_newsletter_subscription_path
  end

  def new
    @newsletter_subscription = NewsletterSubscription.new
  end

  def show
    @newsletter_subscription = NewsletterSubscription.find_by_token(params[:id])
  end

  def confirm
    @newsletter_subscription = NewsletterSubscription.find_by_token(params[:id])

    if @newsletter_subscription.confirmed.nil?
      @newsletter_subscription.confirmed = true
      @newsletter_subscription.confirmed_at = Time.now
      @newsletter_subscription.save
      redirect_to root_path, :flash => { :email => t('newsletter_subscriptions.messages.notices.confirmed', :email => articalize(@newsletter_subscription.email)) }
    else
      redirect_to root_path, notice: t('newsletter_subscriptions.messages.notices.already_confirmed')
    end
  end

  def create
    @newsletter_subscription = NewsletterSubscription.new(params[:newsletter_subscription])

    if user_signed_in?
      @newsletter_subscription.user = current_user
    end

    respond_to do |format|
      if @newsletter_subscription.save
        NewsletterSubscriptionMailer.newsletter_subscription_confirmation(@newsletter_subscription).deliver
        format.html { redirect_to root_path, :flash => { :email => t('newsletter_subscriptions.messages.notices.created', :email => articalize(@newsletter_subscription.email)) } }
      else
        format.html { render action: "new" }
      end
    end
  end

  def confirm_unsubscribe
    @newsletter_subscription = NewsletterSubscription.find(params[:id])

    respond_to do |format|
      format.js
    end
  end

  def destroy
    @newsletter_subscription = NewsletterSubscription.find(params[:id])
    @newsletter_subscription.destroy

    respond_to do |format|
      format.html { redirect_to root_url, notice: t('newsletter_subscriptions.messages.notices.destroyed') }
      format.json { head :no_content }
    end
  end
end
