class Admin::ProductsController < ApplicationController
  before_filter :authenticate_user!, :show_not_found_for_non_admins
  before_action :set_product, only: [:edit, :update, :destroy]
  layout "admin"

  include Concerns::Administrable

  def index
    @products = Product.available.paginate(:per_page => 25, :page => params[:page])
    authorize @products
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_products_path, notice: t('admin.products.messages.notices.created') }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to admin_products_path, notice: t('admin.products.messages.notices.updated') }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  def destroy
    @product.update available: false

    respond_to do |format|
      format.html { redirect_to admin_products_path, notice: t('admin.products.messages.notices.destroyed') }
    end
  end

  private
    def set_product
      @product = Product.where(id: params[:id], available: true).first
      authorize @product
    end

    def product_params
       params.require(:product).permit(
         :name,
         :public,
         :price,
         :delivery_price,
         :free_delivery_from_quantity,
         :description,
         :excerpt,
         :image_url,
         :external_url
       )
    end
end
