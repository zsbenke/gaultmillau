class Admin::RestaurantReservationsController < ApplicationController
  include Concerns::Administrable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @restaurant_reservations = RestaurantReservation.filter_by_meta(params[:search]).paginate(:per_page => 25, :page => params[:page])
    authorize @restaurant_reservations
  end

  def show
    @restaurant_reservation = RestaurantReservation.find(params[:id])
    authorize @restaurant_reservation
  end

  def destroy
    @restaurant_reservation = RestaurantReservation.find(params[:id])
    authorize @restaurant_reservation

    @restaurant_reservation.destroy

    respond_to do |format|
      format.html { redirect_to admin_restaurant_reservations_url, notice: t('admin.restaurant_reservations.messages.notices.destroyed') }
      format.js
      format.json { head :no_content }
    end
  end
end
