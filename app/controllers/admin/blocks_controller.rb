class Admin::BlocksController < ApplicationController
  include Concerns::Administrable
  before_filter :authenticate_user!, :show_not_found_for_non_admins

  def show
    @post = Post.find(params[:post_id])
    @block = @post.blocks.find(params[:id])
    authorize @post

    respond_to do |format|
      format.js
    end
  end

  def new
    @post = Post.find(params[:post_id])
    @block = @post.blocks.new
    authorize @post

    respond_to do |format|
      format.js
    end
  end

  def edit
    @post = Post.find(params[:post_id])
    @block = @post.blocks.find(params[:id])
    authorize @post

    respond_to do |format|
      format.js
    end
  end

  def create
    @post = Post.find(params[:post_id])
    @block = @post.blocks.new(block_params)
    authorize @post

    @block.save
    @block.move_to_bottom
    respond_to do |format|
      format.js
    end
  end

  def update
    @post = Post.find(params[:post_id])
    @block = @post.blocks.find(params[:id])
    authorize @post

    @block.update(block_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @block = @post.blocks.find(params[:id])
    authorize @post

    if @block.destroy
      respond_to do |format|
        format.js
      end
    end
  end

  def sort
    params[:block].each_with_index do |id, index|
      Block.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

  private
    def block_params
      params.require(:block).permit(:content, :kind, :attachment_id, :css_class, :restaurant_id, :post_id)
    end
end
