class Admin::CommentsController < ApplicationController
  include Concerns::Administrable
  include Concerns::Commentable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @comments = Comment.filter_by_meta(params[:search]).paginate(:per_page => 25, :page => params[:page])
    authorize @comments

    current_user.unread_comments = 0
    current_user.save

    respond_to do |format|
      format.html
    end
  end

  def edit
    @comment = Comment.find(params[:id])
    authorize @comment

    if current_user.unread_comments != 0
      current_user.unread_comments -= 1
      current_user.save
    end
  end

  def update
    @comment = Comment.find(params[:id])
    authorize @comment

    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to admin_comments_url, notice: t('admin.comments.messages.notices.updated') }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    authorize @comment
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to admin_comments_url, notice: t('admin.comments.messages.notices.destroyed') }
      format.js
      format.json { head :no_content }
    end
  end
end
