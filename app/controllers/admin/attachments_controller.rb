class Admin::AttachmentsController < ApplicationController
  include Concerns::Administrable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @attachment = Attachment.new
    @attachments = Attachment.by_categories(params[:category]).order('created_at DESC').paginate(:per_page => 28, :page => params[:page])
    @attachments_picker = Attachment.by_keyword(params[:keyword]).by_categories(params[:category]).order('created_at DESC').paginate(:per_page => 28, :page => params[:page])
    @attachment_months = @attachments_picker.group_by { |a| a.created_at.beginning_of_month }

    session[:last_attachment_picker_filter] = params[:category]

    authorize @attachments

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @attachments }
      format.js
    end
  end

  def show
    @attachment = Attachment.find(params[:id])
    authorize @attachment

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @attachment }
    end
  end

  def new
    @attachment = Attachment.new
    authorize @attachment

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @attachment }
    end
  end

  def edit
    @attachment = Attachment.find(params[:id])
    authorize @attachment
  end

  def create
    @attachment = Attachment.new(attachment_params)
    authorize @attachment

    respond_to do |format|
      if @attachment.save
        format.html { redirect_to admin_attachments_url(:category => @attachment.category), notice: t('admin.attachments.messages.notices.created') }
        format.json { render json: @attachment, status: :created, location: @attachment }
        if params[:dropped_from_editor]
          @block = Block.new
          @block.kind = 'image'
          @block.css_class = 'default'
          @block.content = ' '
          @block.attachment = @attachment
          @block.post_id = params[:post_id]
          @block.save
          format.js
        end
      else
        format.html { render action: "new" }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @attachment = Attachment.find(params[:id])
    authorize @attachment

    respond_to do |format|
      if @attachment.update(attachment_params)
        format.html { redirect_to admin_attachments_url(:category => @attachment.category), notice: t('admin.attachments.messages.notices.updated') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @attachment = Attachment.find(params[:id])
    authorize @attachment
    @attachment.destroy

    respond_to do |format|
      format.html { redirect_to admin_attachments_url, notice: t('admin.attachments.messages.notices.destroyed') }
      format.json { head :no_content }
    end
  end

  private
    def attachment_params
      params.require(:attachment).permit(:title, :attachment, :category)
    end
end
