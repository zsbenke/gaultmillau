class Admin::UsersController < ApplicationController
  include Concerns::Administrable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @users = User.filter_by_meta(params[:search]).paginate(:per_page => 25, :page => params[:page])

    authorize @users

    respond_to do |format|
      format.html
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize @user
  end

  def update
    @user = User.find(params[:id])
    authorize @user

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admin_users_url, notice: t('admin.users.messages.notices.updated') }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    authorize @user
    @user.destroy

    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: t('admin.users.messages.notices.destroyed') }
      format.json { head :no_content }
    end
  end

  def manual_confirm
    @user = User.find(params[:id])
    authorize @user
    @user.confirm!

    respond_to do |format|
      format.js
    end
  end

  private
    def user_params
      params.require(:user).permit(:role_admin, :role_editor, :role_senior_editor, :role_accountant)
    end
end
