class Admin::HomeBlocksController < ApplicationController
  include Concerns::Administrable
  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @main_post_home_block = HomeBlock.find_by_kind('main_post')
    authorize @main_post_home_block
    @main_post = Post.find(@main_post_home_block.post.id)
    @featured_blocks = HomeBlock.includes(post: [:attachment]).featured_blocks
  end

  def show
    @home_block = HomeBlock.find(params[:id])
    authorize @home_block

    respond_to do |format|
      format.js
    end
  end

  def show_main_post
    @home_block = HomeBlock.find(params[:id])
    authorize @home_block

    respond_to do |format|
      format.js
    end
  end

  def new
    if !params[:insert_before].blank?
      originated_block = HomeBlock.find params[:insert_before]
    end

    @home_block = HomeBlock.new :kind => 'featured_post', :post => Post.last_blog_post
    authorize @home_block


    respond_to do |format|
      if @home_block.save
        if !params[:insert_before].blank?
          @home_block.insert_at originated_block.position
        else
          @home_block.move_to_bottom
        end
        format.js
      end
    end
  end

  def edit
    @home_block = HomeBlock.find(params[:id])
    authorize @home_block

    respond_to do |format|
      format.js
    end
  end

  def update
    @home_block = HomeBlock.find(params[:id])
    authorize @home_block

    @home_block.update(home_block_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @home_block = HomeBlock.find(params[:id])
    authorize @home_block

    if @home_block.destroy
      respond_to do |format|
        format.js
      end
    end
  end

  def sort
    params[:home_block].each_with_index do |id, index|
      HomeBlock.update_all({position: index+1}, {id: id})
    end
    render nothing: true
  end

  private
    def home_block_params
      params.require(:home_block).permit(:kind, :has_featured_image, :image_align, :post_id, :image_url, :title, :post, :style_type)
    end
end
