class Admin::PostsController < ApplicationController
  include Concerns::Administrable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'

  def index
    if params[:tag].blank?
      @posts = Post.includes(:user).by_categories(params[:category]).filter_by_meta(params[:search]).by_author(params[:author]).reorder('published_at DESC').paginate(:per_page => 25, :page => params[:page])
    else
      @posts = Post.includes(:user).by_categories(params[:category]).filter_by_meta(params[:search]).tagged_with(params[:tag]).reorder('published_at DESC').paginate(:per_page => 25, :page => params[:page])
    end
    authorize @posts

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  def new
    @post = Post.new
    authorize @post

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  def edit
    @post = Post.find(params[:id])
    authorize @post
  end

  def create
    @post = Post.new(post_params)
    authorize @post
    @post.user = current_user
    @post.published_at = Time.now

    respond_to do |format|
      if @post.save
        format.html { redirect_to edit_admin_post_url(@post, :category => params[:category]), notice: t('admin.posts.messages.notices.created') }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @post = Post.find(params[:id])
    authorize @post

    if params[:post][:public] && !@post.public
      @post.published_at = Time.now
    end

    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to admin_posts_url, notice: t('admin.posts.messages.notices.updated') }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post = Post.find(params[:id])
    category = @post.category
    authorize @post
    @post.destroy

    respond_to do |format|
      format.html { redirect_to admin_posts_path(:category => category), notice: t('admin.posts.messages.notices.destroyed') }
      format.json { head :no_content }
    end
  end

  def recipes_list
    @recipes = Post.by_categories('receptek')

    respond_to do |format|
      format.js
    end
  end

  def edit_post_meta
    @post = Post.find(params[:id])

    respond_to do |format|
      format.js
    end
  end

  private
    def post_params
      params.require(:post).permit(:category, :title, :public, :created_at, :user_id, :tag_list, :excerpt, :published_at, :attachment_id, :post_class, :not_commentable)
    end
end
