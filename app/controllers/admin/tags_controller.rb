class Admin::TagsController < ApplicationController
  include Concerns::Administrable
  before_filter :authenticate_user!, :show_not_found_for_non_admins, :raise_error_for_non_admins
  layout "admin"

  def index
    @tags = find_tags params[:search]
  end

  def edit
    @tag = ActsAsTaggableOn::Tag.find(params[:id])

    respond_to do |format|
      format.js
    end
  end

  def edit
    @tag = ActsAsTaggableOn::Tag.find(params[:id])

    respond_to do |format|
      format.js
    end
  end

  def update
    @tag = ActsAsTaggableOn::Tag.find(params[:id])

    @tag.update(tag_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @tag = ActsAsTaggableOn::Tag.find(params[:id])

    @tag.taggings.each do |tagging|
      tagging.destroy
    end

    if @tag.destroy
      respond_to do |format|
        format.js
      end
    end
  end

  private
    def raise_error_for_non_admins
      if !current_user.role_admin && !current_user.role_senior_editor
        raise Pundit::NotAuthorizedError, "not allowed"
      end
    end

    def find_tags keyword
      if !keyword.blank?
        ActsAsTaggableOn::Tag.where('name @@ :q', q: keyword).order('unaccent(name) ASC').group_by { |u| u.name[0] }
      else
        ActsAsTaggableOn::Tag.scoped.order('unaccent(name) ASC').group_by { |u| u.name[0] }
      end
    end

    def tag_params
      params.require(:tag).permit(:name)
    end
end
