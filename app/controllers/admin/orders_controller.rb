class Admin::OrdersController < ApplicationController
  include Concerns::Administrable
  include Orderable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    @orders = Order.filter_by_meta(params[:search]).by_payment_method(params[:payment_method]).by_status(params[:status]).paginate(:per_page => 25, :page => params[:page])
    @orders_not_paginated = Order.filter_by_meta(params[:search]).by_payment_method(params[:payment_method]).by_status(params[:status])
    authorize @orders
    respond_to do |format|
      format.html
      format.xls do
        stream = render_to_string(:template=>"admin/orders/index")
        send_data stream, filename: "gaultmillau_rendelesek_#{Time.now.strftime('%Y_%m_%d_%H_%M')}.xls", type: "application/xls"
      end
    end
  end

  def edit
    @order = Order.find(params[:id])
    authorize @order
  end

  def update
    @order = Order.find(params[:id])
    authorize @order
    @order.attributes = order_params

    respond_to do |format|
      if @order.save(:validate => false)
        format.html { redirect_to admin_orders_url, notice: t('admin.orders.messages.notices.updated') }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @order = Order.find(params[:id])
    authorize @order
    @order.destroy

    respond_to do |format|
      format.html { redirect_to admin_orders_url, notice: t('admin.orders.messages.notices.destroyed') }
      format.json { head :no_content }
    end
  end
end
