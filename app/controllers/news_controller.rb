class NewsController < ApplicationController
  def index
    @news = Post.includes(:user, :attachment).news_public()
    @news = @news.tagged_with(params[:tag]) if params[:tag].present?
    @news = @news.order('published_at DESC').paginate(:per_page => 12, :page => params[:page])

    respond_to do |format|
      format.html
      format.json { render json: @news }
    end
  end

  def show
    if (user_signed_in? && current_user.role_admin) ||
       (user_signed_in? && current_user.role_editor) ||
       (user_signed_in? && current_user.role_senior_editor)
      @news = Post.news_posts.find_by_url(params[:id])
    else
      @news = Post.public.find_by_url(params[:id])
    end
  end
end
