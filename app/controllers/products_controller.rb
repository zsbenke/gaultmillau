class ProductsController < ApplicationController
  include ApplicationHelper

  def index
    @products = Product.public
  end

  def show
    @product = Product.public.find params[:id]
    return redirect_to(@product.external_url) if @product.external_url.present?
    redirect_to new_product_order_path @product
  end
end
