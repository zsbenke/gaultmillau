module Concerns
  module Administrable
    extend ActiveSupport::Concern

    def show_not_found_for_non_admins
      not_found unless current_user.role_admin || current_user.role_senior_editor || current_user.role_editor || current_user.role_accountant
    end

    private
      def not_found
        puts "Not authorized access to admin: #{how_is_trying_to_access?}"
        raise ActionController::RoutingError.new('Not Found')
      end

      def how_is_trying_to_access?
        if current_user
          "#{current_user.username}, #{session[:current_user_ip]}"
        else
          "anonymus, #{session[:current_user_ip]}"
        end
      end
  end
end
