module Orderable
  extend ActiveSupport::Concern

  private
    def setup_order
      @product = Product.public.where(:id => params[:product_id]).last
      redirect_to @product.external_url if @product.external_url.present?
    end

    def order_params
      params.require(:order).permit(:address, :city, :email, :full_name, :phone, :postcode, :product_count, :billing_postcode, :billing_city, :billing_address, :billing_address_same_as_order_address, :billing_name, :billing_phone, :status, :post_identifier, :paid, :comment, :vat_number, :payment_method)
    end
end
