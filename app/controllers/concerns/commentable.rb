module Concerns
  module Commentable
    extend ActiveSupport::Concern

    private
      def comment_params
        params.require(:comment).permit(:body)
      end
  end
end
