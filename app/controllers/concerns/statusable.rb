module Concerns
  module Statusable
    extend ActiveSupport::Concern

    def update_unread_comments_for_admins
      User.admins.each do |user|
        if user.unread_comments == nil
          user.unread_comments = 1
          user.save
        else
          user.unread_comments += 1
          user.save
        end
      end
    end
  end
end
