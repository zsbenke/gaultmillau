class FeedbacksController < ApplicationController
  def new
    @feedback = Feedback.new
  end

  def create
    @feedback = Feedback.new(feedback_params)

    respond_to do |format|
      if @feedback.save
        FeedbackMailer.contact(@feedback, session[:current_user_ip]).deliver
        format.html { redirect_to root_path, :flash => { :email => t('feedbacks.messages.notices.created').html_safe } }
      else
        flash[:notice] = t('feedbacks.messages.notices.send_failed')
        format.html { render action: "new" }
      end
    end
  end

  private
    def feedback_params
      params.require(:feedback).permit(:content, :name, :phone, :address, :captcha, :is_error_reporting, :record_kind, :record_id, :record_name)
    end
end
