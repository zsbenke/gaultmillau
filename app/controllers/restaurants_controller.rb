class RestaurantsController < ApplicationController
  def index
    session[:last_restaurants_filter] = params
    search = params[:search].strip unless params[:search].nil?
    @restaurants = Restaurant.public.
      by_keyword(search).
      by_country(params[:country]).
      by_rating(params[:rating]).
      by_tags(params[:tag]).
      by_open_on_monday(params[:open_on_monday]).
      by_open_on_sunday(params[:open_on_sunday]).
      reorder("restaurants.position ASC")
  end

  def show
    @restaurant = Restaurant.find(params[:id])

    if !@restaurant.public || @restaurant.year != CONFIG_GM_YEAR
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def best_of_map
    search = params[:search].strip unless params[:search].nil?
    @restaurants = Restaurant.public.by_keyword(search).by_rating(['best_of']).order("restaurants.created_at DESC")
  end

  def autocomplete_restaurant_search_cache_name
    term = params[:term]
    json = []

    if term.blank?
      json = ['minden hely', 'éttermek', 'pop helyek', 'nyitva hétfőn', 'nyitva vasárnap', 'olasz', 'francia', 'orosz', 'vietnámi', 'ázsiai', 'japán', 'kínai', 'thai', 'spanyol', 'mexikói', 'hagyományos csárda', 'nemzetközi', 'magyar', 'újító magyar', 'bisztró', 'ázsiai', 'fine dining', 'ázsiai-bolt', 'borbár', 'borozó', 'büfé', 'burger', 'csevap', 'csokoládé', 'cukrászda', 'delikát bolt', 'ételbár', 'étkezde', 'fagylaltozó', 'fánk', 'girosz', 'grill', 'hal', 'halsütöde', 'hentes', 'kávézó', 'kávé' 'keksz', 'kifőzde', 'kolbász', 'lángos', 'pastrami', 'pékség', 'pho', 'piac', 'pizza', 'reggeliző', 'rétes', 'sör', 'sushi', 'tapasz', 'tea']
      json = json.map { |cache| {  :label => cache, :value => cache  } }
    else
      restaurant_search_caches = RestaurantSearchCache.where('name ILIKE ?', "#{term}%").order("rank DESC").limit(10)
      json = restaurant_search_caches.map { |cache| { :label => cache.name, :value => cache.name } }
    end
    render :json => json
  end
end
