class RestaurantReservationsController < ApplicationController
  include ApplicationHelper

  def index
    redirect_to restaurants_path, notice: t('restaurant_reservations.messages.notices.choose_restaurant')
  end

  def new
    @restaurant_reservation = RestaurantReservation.new

    if params[:book_to].blank?
      redirect_to restaurants_path, notice: t('restaurant_reservations.messages.notices.choose_restaurant')
    else
      respond_to do |format|
        format.html
      end
    end
  end

  def create
    @restaurant_reservation = RestaurantReservation.new(restaurant_reservation_params)
    if user_signed_in?
      @restaurant_reservation.user = current_user

      if @restaurant_reservation.save_phone_to_profile == true
        user = User.find current_user.id
        user.phone = @restaurant_reservation.phone
        user.save
      end
    end

    respond_to do |format|
      if @restaurant_reservation.save
        RestaurantReservationMailer.restaurant_reservation_booking(@restaurant_reservation).deliver
        format.html {
          if @restaurant_reservation.save_to_users && !user_signed_in?
            redirect_to new_user_registration_path(:email => @restaurant_reservation.email), :flash => { :email => t('restaurant_reservations.messages.notices.created', :restaurant_title => @restaurant_reservation.restaurant.title, :email => articalize(@restaurant_reservation.email, 'uppercase')).html_safe }
          else
            redirect_to restaurant_path(@restaurant_reservation.restaurant), :flash => { :email => t('restaurant_reservations.messages.notices.created', :restaurant_title => @restaurant_reservation.restaurant.title, :email => articalize(@restaurant_reservation.email, 'uppercase')).html_safe }
          end
        }
      else
        format.html { render action: "new" }
      end
    end
  end

  def confirm
    @restaurant_reservation = RestaurantReservation.find_by_token(params[:id])

    if @restaurant_reservation.confirmed.nil?
      @restaurant_reservation.confirmed = true
      @restaurant_reservation.confirmed_at = Time.now
      @restaurant_reservation.save
      RestaurantReservationMailer.restaurant_reservation_confirmed(@restaurant_reservation).deliver
      redirect_to root_path, :flash => { :email => t('restaurant_reservations.messages.notices.confirmed', :email => articalize(@restaurant_reservation.email)) }
    else
      redirect_to root_path, notice: t('restaurant_reservations.messages.notices.already_confirmed')
    end
  end

  private
    def restaurant_reservation_params
      params.require(:restaurant_reservation).permit(:comment, :email, :name, :feedback_type, :number_of_persons, :phone, :book_at, :book_time, :restaurant_id, :save_phone_to_profile, :confirmed, :confirmed_at, :token, :save_to_users)
    end
end
