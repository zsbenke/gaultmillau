class AdminController < ApplicationController
  include Concerns::Administrable

  before_filter :authenticate_user!, :show_not_found_for_non_admins
  layout "admin"

  def index
    if current_user.role_admin || current_user.role_senior_editor || current_user.role_editor
      redirect_to admin_posts_path
    else
      redirect_to admin_orders_path
    end
  end
end