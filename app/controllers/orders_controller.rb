class OrdersController < ApplicationController
  before_action :setup_order, only: [:index, :new, :create]

  include ApplicationHelper
  include Orderable

  def index
    redirect_to new_product_order_path @product
  end

  def show
    if params[:token]
      @order = Order.where(:token => params[:token]).first
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def new
    @order = @product.orders.new
  end

  def create
    @order = @product.orders.new(order_params)

    if user_signed_in?
      @order.user = current_user
    end

    respond_to do |format|
      if @order.save
        OrderMailer.order_confirmation(@order).deliver
        if @order.payment_method == 'transfer'
          format.html { redirect_to order_path(@order.token) }
        else
          format.html { redirect_to root_path, :flash => { :email => t('orders.messages.notices.created', :email => articalize(@order.email, 'uppercase')).html_safe } }
        end
      else
        format.html { render action: "new" }
      end
    end
  end
end
