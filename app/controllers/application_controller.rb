class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery
  before_filter :mailer_set_url_options, :supported_browser?, :set_current_user_ip, :write_current_user_to_log, :set_gm_guide_product
  before_filter :configure_permitted_parameters, if: :devise_controller?
  if !ENV['HTTP_BASIC_AUTH_NAME'].blank? && !ENV['HTTP_BASIC_AUTH_PASSWORD'].blank?
    http_basic_authenticate_with name: ENV['HTTP_BASIC_AUTH_NAME'], password: ENV['HTTP_BASIC_AUTH_PASSWORD']
  end
  before_filter :coming_soon_page

  def set_current_user_ip
    session[:current_user_ip] = request.remote_ip
  end

  def coming_soon_page
    if CONFIG_SHOW_COMING_SOON
      unless user_signed_in? &&
             current_user.admin? ||
             controller_name == 'sessions'
        redirect_to '/coming-soon.html'
      end
    end
  end

  def mailer_set_url_options
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def supported_browser?
    user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT'])
    if !user_agent.version.nil?
      if (user_agent.browser == 'Internet Explorer' && user_agent.version < '9.0') ||
         (user_agent.browser == 'Safari' && user_agent.version < '5.0') ||
         (user_agent.browser == 'Firefox' && user_agent.version < '4.0') ||
         (user_agent.browser == 'Chrome' && user_agent.version < '14.0')
        redirect_to '/not-supported.html'
      end
    end
  end

  def write_current_user_to_log
    if current_user
      puts "Currently signed in user: #{current_user.username}, #{current_user.current_sign_in_ip} - #{controller_name}/#{action_name} - #{request.protocol}#{request.host_with_port}#{request.fullpath}"
    end
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) << :username
      devise_parameter_sanitizer.for(:account_update) << [:username, :phone]
    end

    def set_gm_guide_product
      @gm_guide_product = Product.find_by_name('2017-es Gault&Millau Magyarország kalauz')
    end
end
