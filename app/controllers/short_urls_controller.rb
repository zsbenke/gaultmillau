class ShortUrlsController < ApplicationController
  include ApplicationHelper

  def show
    @post = Post.find params[:id]

    redirect_to get_post_path_for(@post)
  end
end
