class FavouritesController < ApplicationController
  def index
    @favourites = Favourite.for_user(current_user)
    respond_to do |format|
      format.js
    end
  end

  def create
    if user_signed_in?
      @favourite = Favourite.new(favourite_params)
      @favourite.user = current_user

      respond_to do |format|
        if @favourite.save
          format.js
        end
      end
    end
  end

  def update
    @favourite = Favourite.find(params[:id])
    @favourite.destroy

    respond_to do |format|
      format.js
    end
  end

  private
    def favourite_params
      params.require(:favourite).permit(:kind, :record_id)
    end
end