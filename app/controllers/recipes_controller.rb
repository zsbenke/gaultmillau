class RecipesController < ApplicationController
  def index
    @recipes = Post.recipes_public().filter_by_meta(params[:search]).recipes_tagged_with(params[:tag]).recipes_tagged_with(params[:kitchen]).reorder('unaccent(title) ASC').paginate(:per_page => 24, :page => params[:page])
    respond_to do |format|
      format.html
      format.json { render json: @recipes }
    end
  end

  def show
    if (user_signed_in? && current_user.role_admin) ||
       (user_signed_in? && current_user.role_editor) ||
       (user_signed_in? && current_user.role_senior_editor)
      @recipe = Post.recipes_posts.find_by_url(params[:id])
    else
      @recipe = Post.recipes_public.find_by_url(params[:id])
    end
  end
end
