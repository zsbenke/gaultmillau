(($) ->
  $.fn.flash_notice = ->
    if $("[data-behavior~=flash-notice]").length > 0
      $("[data-behavior~=flash-notice]").on "click", "[data-behavior~=flash-notice-close]", ->
        $(this).parents().filter("[data-behavior~=flash-notice]").fadeOut 150
        false

) jQuery
