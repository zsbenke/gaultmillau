(($) ->
  $.fn.restaurant_reservations = ->
    if $('[data-behavior~=show-restaurant-reservation-details]').length > 0
      $('[data-behavior~=show-restaurant-reservation-details]').on 'click', ->
        restaurant_reservation_id = $(this).data('restaurant-reservation-id')
        $("[data-restaurant-reservation-details~=" + restaurant_reservation_id + "]").slideToggle(150)
        $(this).toggleClass('opened')
        false

) jQuery
