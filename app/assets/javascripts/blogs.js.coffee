(($) ->
  $.fn.blogs = ->
    if $("[data-behavior~=tags-popover-toggle]").length > 0
      $('[data-behavior~=tags-popover-toggle]').on "click", ->
        $('.popover').removeClass('show');
        $('[data-behavior~=tags-popover]').addClass('show');
        false

    if $("[data-behavior~=post-tags-popover-toggle]").length > 0
      $('[data-behavior~=post-tags-popover-toggle]').on "click", ->
        $('.popover').removeClass('show');
        $(this).parent().next('[data-behavior~=post-tags-popover]').addClass('show');
        false

    if $("[data-behavior~=table-of-contents-popover-toggle]").length > 0
      $('[data-behavior~=table-of-contents-popover-toggle]').on "click", ->
        $('.popover').removeClass('show');
        $('[data-behavior~=table-of-contents-popover]').addClass('show');
        false

) jQuery
