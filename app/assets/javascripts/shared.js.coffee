loader = undefined

# page_updates = setInterval(->
#  $.ajax
#    url: $('body').data('updates-path')
#    dataType: "script"
# , 60000)

window.initialize_application = ->
  $.fn.flash_notice()
  $.fn.users()
  $.fn.blogs()
  $.fn.orders()
  $.fn.restaurant_reservations()
  $.fn.admin()
  $.fn.database()

  initialize_map() if $("[data-behavior~=map-canvas]").length > 0

  hash_id = window.location.hash;

  $('[data-behavior~=post-tiles]').masonry
    itemSelector: '.post-tile'
    isResizable: true
    columnWidth: (containerWidth) ->
      size = $(window).width()
      if size < 740
        containerWidth / 1
      else if size < 1150
        containerWidth / 2
      else if size < 1540
        containerWidth / 3
      else if size <=  1920
        containerWidth / 4
      else
        containerWidth / 5

  $('[data-behavior~=post-tiles]').imagesLoaded ->
    $('[data-behavior~=post-tiles]').masonry("reload")

  if hash_id != false && hash_id != '#comments'
    $(hash_id).effect("highlight", {color: '#feffe6'}, 1000);

  $("[data-behavior~=copy-paste-field]").on "click", ->
    $(this).select()

  $("[data-behavior~=hamburger]").on "click", ->
    $('[data-behavior~=navigation]').toggleClass('show');
    false

  $('.error').on 'blur', ->
    $(this).parents('.field_with_errors').next().fadeOut 150, ->
      $(this).parents('.error').removeClass('error')

  $('html').on "click", ->
    $('.popover').removeClass('show')

  $('[data-behavior~=print-button]').on "click", ->
    window.print()
    false

  if $('[data-behavior~=close-best-of-modal]').length > 0
    $('[data-behavior~=close-best-of-modal]').on 'click', ->
      $('[data-behavior~=best-of-modal]').fadeOut(150)
      false

  $("form[data-behavior~=search-form]").on "submit", ->
    Turbolinks.visit @action + '?' + $(this).serialize()
    false

ready = ->
  window.initialize_application()

$(document).ready ready
$(document).on "page:load", ->
  clearTimeout(loader)
  ready()
  $("html").addClass "loaded"

$(document).on "page:fetch", ->
  $("html").removeClass "loaded"

(->
  bindFacebookEvents = undefined
  fb_events_bound = undefined
  fb_root = undefined
  initializeFacebookSDK = undefined
  loadFacebookSDK = undefined
  restoreFacebookRoot = undefined
  saveFacebookRoot = undefined
  fb_root = null
  fb_events_bound = false
  $ ->
    loadFacebookSDK()
    bindFacebookEvents()  unless fb_events_bound

  bindFacebookEvents = ->
    $(document).on("page:fetch", saveFacebookRoot).on("page:change", restoreFacebookRoot).on "page:load", ->
      (if typeof FB isnt "undefined" and FB isnt null then FB.XFBML.parse() else undefined)

    fb_events_bound = true

  saveFacebookRoot = ->
    fb_root = $("#fb-root").detach()

  restoreFacebookRoot = ->
    if $("#fb-root").length > 0
      $("#fb-root").replaceWith fb_root
    else
      $("body").append fb_root

  loadFacebookSDK = ->
    window.fbAsyncInit = initializeFacebookSDK
    $.getScript "//connect.facebook.net/hu_HU/all.js#xfbml=1"

  initializeFacebookSDK = ->
    FB.init
      status: true
      cookie: true
      xfbml: true
    FB.Event.subscribe 'xfbml.render', (response)->
      $('[data-behavior~=post-tiles]').masonry("reload")

).call this
