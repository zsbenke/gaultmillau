(($) ->
  $.fn.admin = ->
    if $("form[data-behavior~=table-filter]").length > 0
      $("form[data-behavior~=table-filter]").on "submit", ->
        Turbolinks.visit @action + '?' + $(this).serialize()
        false

    if $("form[data-behavior~=table-form]")
      $("form[data-behavior~=table-form] input, form[data-behavior~=table-form] select").on "change", ->
        $(this).parents("form[data-behavior~=table-form]").submit()

    if $("form[data-behavior~=table-filter]")
      $("form[data-behavior~=table-filter] input, form[data-behavior~=table-filter] select").on "change", ->
        $(this).parents("form[data-behavior~=table-filter]").submit()

    if $("[data-behavior~=home-blocks]").length > 0
      $("[data-behavior~=home-blocks]").sortable
        handle: ".handle"
        scroll: true
        revert: 150
        opacity: 0.75
        delay: 150
        distance: 20
        tolerance: "pointer"
        items: "li:not(.not-sortable)"
        cancel: ".not-sortable"
        start: (event, ui) ->
          ui.placeholder.height(ui.helper.outerHeight())
          ui.item.addClass("dragging").removeClass "post-tile"
          ui.placeholder.addClass "bigun"  if ui.item.hasClass("bigun")
          ui.item.parent().masonry "reload"

        change: (event, ui) ->
          ui.item.parent().masonry "reload"

        stop: (event, ui) ->
          ui.item.removeClass("dragging").addClass "post-tile"
          ui.item.parent().masonry "reload"

        update: ->
          $.post $(this).data("update-url"), $(this).sortable("serialize")

    if $("[data-behavior~=post-blocks]").length > 0
      $('[data-behavior~=outline-view]').on 'click', ->
        $('[data-behavior~=post-blocks]').toggleClass('outline');
        false

      $("[data-behavior~=post-blocks]").sortable
        handle: ".handle"
        scroll: true
        revert: 150
        opacity: 0.75
        delay: 150
        distance: 20
        items: "li:not(.not-sortable)"
        cancel: ".not-sortable"
        start: ->
          $(this).addClass('dragged');
        stop: ->
          $(this).removeClass('dragged');
        update: ->
          $.post $(this).data("update-url"), $(this).sortable("serialize")

    $("[data-behavior~=attachment-uploader]").fileupload
      dropZone: $("[data-behavior~=attachment-dropzone]")
      dataType: "script"
      add: (e, data) ->
        data.submit()

      start: (e) ->
        $("[data-behavior~=attachment-progress]").addClass "in-progress"

      dragover: (e) ->
        drop_zone = $("[data-behavior~=attachment-dropzone]")
        timeout = window.drop_zone_timeout
        unless timeout
          drop_zone.addClass "dragged"
        else
          clearTimeout timeout
        window.drop_zone_timeout = setTimeout(->
          window.drop_zone_timeout = null
          drop_zone.removeClass "dragged"
        , 100)

      stop: (e, data) ->
        $("[data-behavior~=attachment-progress]").removeClass "in-progress"
        if !$('#dropped_from_editor').length > 0
          Turbolinks.visit location.href

      progressall: (e, data) ->
        progress = parseInt(data.loaded / data.total * 100, 10)
        $("[data-behavior~=attachment-progress-bar]").attr "style", "width: " + progress + "%"
        if progress is 100
          $("[data-behavior~=attachment-progress-info]").html "Feltöltés befejezése…"
        else
          $("[data-behavior~=attachment-progress-info]").html progress + "%"

    $("body").on "ajax:beforeSend", "[data-remote~=true]", ->
      $("html").removeClass "loaded"

) jQuery
