(($) ->
  $.fn.orders = ->
    if $('[data-behavior~=show-order-details]').length > 0
      $('[data-behavior~=show-order-details]').on 'click', ->
        order_id = $(this).data('order-id')
        $("[data-order-details~=" + order_id + "]").slideToggle(150)
        $(this).toggleClass('opened')
        false

    if $('[data-behavior~=billing-address-toggle]').length > 0
      is_billing_address_toggle_checked = $('[data-behavior~=billing-address-toggle]').attr('checked')
      $('[data-behavior~=billing-address-toggle]').on 'change', ->
        $('[data-behavior~=billing-address]').slideToggle(250)

      if is_billing_address_toggle_checked == 'checked'
        $('[data-behavior~=billing-address]').hide()

) jQuery
