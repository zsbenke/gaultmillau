(($) ->
  $.fn.database = ->
    split = (val) ->
      val.split(" ")
    extractLast = (term) ->
      split(term).pop()

    if $("form[data-behavior~=database-filter]").length > 0
      $("form[data-behavior~=database-filter]").on "submit", ->
        Turbolinks.visit @action + '?' + $(this).serialize()
        false

    if $("[data-behavior~=rating-help-popover-toggle]").length > 0
      $('[data-behavior~=rating-help-popover-toggle]').on "click", ->
        $('.popover').removeClass('show');
        $('[data-behavior~=rating-help-popover]').addClass('show');
        false

    $("[data-behavior~=toggle-last-year-tests]").on "click", ->
      toggle = $(this).data('toggle');
      $(this).toggleClass('opened')
      $(toggle).slideToggle(250)
      false

    $('[data-behavior~=record-tiles]').masonry
      itemSelector: '.result'
      isResizable: true
      columnWidth: (containerWidth) ->
        size = $(window).width()
        if size < 1000
          containerWidth / 2
        else if size < 1500
          containerWidth / 3
        else if size < 2000
          containerWidth / 4

    $('[data-behavior~=record-tiles]').imagesLoaded ->
      $('[data-behavior~=record-tiles]').masonry "reload"

    if $("[data-behavior~=search-input]").length > 0
      $("[data-behavior~=search-input]").on("keydown", (event) ->
        event.preventDefault()  if event.keyCode is $.ui.keyCode.TAB and $(this).autocomplete("instance").menu.active
      ).autocomplete
        minLength: 0
        source: (request, response) ->
          params = term: extractLast($("[data-behavior~=search-input]").val())

          $.getJSON $("[data-behavior~=search-input]").data("autocomplete-path"), params, ->
            if arguments[0].length is 0
              arguments[0] = []

            response.apply null, arguments
            console.log arguments
        focus: ->
          false

        select: (event, ui) ->
          terms = split(@value)
          terms.pop()
          terms.push ui.item.value
          terms.push ""
          @value = terms.join(" ")
          $("form[data-behavior~=database-filter]").submit()
          false

      $("[data-behavior~=search-input]").on "focus", (event) ->
        $(this).autocomplete("search", "")
    $('form[data-behavior~=database-filter] select').on 'change', ->
      $("form[data-behavior~=database-filter]").submit()

    $('[data-behavior~=rating-option]').on 'change', ->
      $("form[data-behavior~=database-filter]").submit()
) $
