(($) ->
  $.fn.users = ->
    destroy_user_registration = $("[data-behavior~=destroy-user-registration]")
    if $("form[data-behavior~=edit-user-registration-form]").length > 0
      destroy_user_registration.addClass "hidden"
      $("[data-behavior~=cancel-user-registration]").on "click", ->
        destroy_user_registration.fadeToggle 200
        false

    $("[data-behavior~=user-popover-toggle]").on "click", ->
      $('.popover').removeClass('show');
      $("[data-behavior~=user-popover]").addClass('show')
      false

    $('html').on "click", ->
      $('[data-behavior~=user-popover]').removeClass('show')

) jQuery
