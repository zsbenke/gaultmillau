/* Forms ---------------------- */
fieldset {
  border: 0;
  margin: 0;
  padding: 0;

  legend {
    font-size: 1em;
    padding-bottom: 0.5*$baseScale;
  }
}

textarea {
  resize: vertical;
}

label {
  font-family: $inputFontFamily;
  font-size: $inputFontSize;
  margin-bottom: 0.5*$baseScale;
  line-height: $baseScale;

  &.inline {
    line-height: 1.6*$baseScale;
  }

  &.required {
    font-weight: bold;

    &:before {
      content: "*";
    }
  }

  &.right {
    text-align: right;
  }
}

@media screen and (max-width: $smallBreakPoint) {
  label.right {
    float: none;
    text-align: left;
  }
}

input[type="text"],
input[type="email"],
input[type="password"],
input[type="url"],
input[type="tel"],
input[type="search"],
input[type="number"],
textarea {
  width: 100%;
  padding: 7px 10px;
  font-family: $inputFontFamily;
  font-size: $inputFontSize;
  color: $inputFontColor;
  border: 1px solid $darkBorderColor;
  outline: none;
  @include border-radius($smallBorderRadius);
  @include transition(all 0.15s ease-out);
  @include antialiased;

  &:focus {
    background-color: lighten($blueColor, 30%);
    border-color: $blueColor;
    @include box-shadow(0 1px 10px rgba($blueColor, 0.2));
  }
}

input[type="file"] {
  font-size: $inputFontSize;
}

input[type="submit"], select {
  font-size: $inputFontSize;
}

textarea {
  line-height: $baseScale;
}

@mixin placeholder {
 color: $inputPlaceholderColor;
 @include antialiased;
}

::-webkit-input-placeholder {
 @include placeholder;
}

::-moz-placeholder {
 @include placeholder;
}

:-ms-input-placeholder {
 @include placeholder;
}

input:-moz-placeholder {
 @include placeholder;
}

.checkbox, .radio-button {
  width: 100%;
  font-size: $smallFontSize;
  line-height: 1.45*$baseScale;

  input[type="checkbox"], input[type="radio"] {
    position: relative;
    top: -1px;
    margin-right: 4px;
  }

  .field_with_errors {
    display: inline;
  }
}

select.inline-field {
  display: inline;
  margin-top: 0.3*$baseScale;
}

.blocked-field {
  display: block;
  margin-bottom: 0.5*$baseScale;
}

.info-panel-field {
  .info-panel:last-child {
    margin-bottom: 0;
  }
}

.error {
  input.error {
    background-color: rgba($redColor, 0.1);
    border-color: $redColor;
  }

  small {
    display: block;
    margin-top: -2px;
    margin-bottom: 0.5*$baseScale;
    padding: 4px;
    font-size: $smallFontSize;
    font-family: $inputFontFamily;
    font-weight: bold;
    color: $whiteColor;
    background-color: $redColor;
    @include antialiased;
    @include border-radius(0 0 $smallBorderRadius $smallBorderRadius);

    span {
      display: block;
      margin-bottom: 0.5*$baseScale;

      &:last-child {
        margin-bottom: 0;
      }
    }
  }
}

.data-label {
  display: block;
  width: 100%;
  padding-top: 0.3*$baseScale;
  margin-bottom: 0.5*$baseScale;
  font-family: $inputFontFamily;
  font-size: $smallFontSize;
  line-height: $baseScale;
  color: $secondaryFontColor;

  &.inline {
    display: inline;
  }

  a {
    display: block;
    font-weight: bold;

    &:hover {
      text-decoration: underline;
    }
  }
}

.copy-paste-field {
  padding: 0;
  padding-top: 0.3*$baseScale;
  font-size: $inputFontSize;
  color: $secondaryFontColor;
  background: none;
  border: none;
  @include border-radius(none);
  @include box-shadow(none);
  resize: none;

  &:focus {
    background: none;
    border: none;
    @include border-radius(none);
    @include box-shadow(none);
  }
}

input.keyword-field {
  padding-left: 28px;
  background-image: image-url("icon-magnifying-glass.png");
  background-position: 9px center;
  background-repeat: no-repeat;
  @include image-2x('icon-magnifying-glass@2x.png', 16px, 16px);
}

.form-actions {
  margin-top: $baseScale;
  padding-top: $baseScale;
  font-family: $inputFontFamily;
  font-size: $inputFontSize;
  color: $secondaryFontColor;
  border-top: 1px solid $lightBorderColor;

  &.plain {
    padding-top: 0;
    border-top: none;
  }

  a {
    color: $redColor;
    text-decoration: underline;
  }
}

.select-field {
  padding: 0 18px 0 6px;
  height: 34px;
  margin-top: 0;
  font-family: $inputFontFamily;
  font-size: $inputFontSize;
  line-height: 2.4em;
  color: $inputFontColor;
  background-color: $whiteColor;
  @include background-image(image-url("dropdown-arrows.png"));
  background-position: right center;
  background-repeat: no-repeat;
  border: 1px solid $darkBorderColor;
  @include appearance(none);
  @include border-radius($smallBorderRadius);
  @include text-overflow;

  &:focus {
    background-color: rgba($blueColor, 0.1);
    border-color: $blueColor;
    @include box-shadow(0 1px 10px rgba($blueColor, 0.2));
    outline: none;
  }
}

.button {
  padding: 8px 14px;
  font-family: $fontFamily;
  font-size: $inputFontSize;
  font-weight: bold;
  color: $whiteColor;
  background-color: $secondaryColor;
  border: none;
  outline: none;
  @include transition(all 0.15s ease-out);
  @include border-radius($smallBorderRadius);
  @include appearance(none);
  @include antialiased;

  &:hover {
    color: $whiteColor;
  }

  &:active {
    background-color: darken($secondaryColor, 10%);
    @include box-shadow(inset 0 2px 2px rgba($blackColor, 0.1));
    @include transition(none);
  }

  &.full-width {
    display: inline-block;
    width: 100%;
  }

  &:disabled {
    cursor: normal;
    opacity: 0.5;
    pointer-events: none;
  }
}

.button-panel {
  padding: 0 !important;
  margin: 0;
  list-style: none;

  .button {
    color: $whiteColor !important;
    @include border-radius(0);
    border-right: 1px solid $darkBorderColor;
  }

  li {
    display: inline;
    float: left;

    &:first-child {
      .button {
        @include border-radius($smallBorderRadius 0 0 $smallBorderRadius);
      }
    }

    &:last-child {
      .button {
        @include border-radius(0 $smallBorderRadius $smallBorderRadius 0);
        border-right: none;
      }
    }

    &:only-child {
      .button {
        @include border-radius($smallBorderRadius);
      }
    }
  }
}

.date-time-picker {
  .date-field {
    width: 65% !important;

    input {
      position: relative;
      @include border-top-right-radius(0);
      @include border-bottom-right-radius(0);

      &:focus {
        z-index: 3;
      }
    }
  }

  .time-field {
    margin-left: -1px;
    width: 35% !important;

    select {
      display: inline-block;
      width: 100%;
      @include border-top-left-radius(0);
      @include border-bottom-left-radius(0);

      &:focus {
        z-index: 999;
      }
    }
  }
}

.radio-button-icon, .checkbox-icon {
  display: inline-block;
  width: 32px;
  height: 32px;
  position: relative;

  label {
    display: block;
    width: 32px;
    height: 32px;
    position: relative;
    opacity: 0.25;
    text-indent: -9999px;
    background-position: center center;
    background-repeat: no-repeat;
    @include border-radius($smallBorderRadius);
    @include antialiased;
  }

  &.with-text {
    line-height: 32px;
    text-align: center;
    vertical-align: middle;
    font-weight: bold;
    text-indent: 0;
  }

  input[type="radio"], input[type="checkbox"] {
    display: block;
    position: absolute;
    z-index: 400;
    width: 32px;
    height: 32px;
    opacity: 0;
    cursor: pointer;
    @include appearance(none);

    &:checked ~ label {
      opacity: 1;
    }
  }
}
