jQuery(function($){
  $.datepicker.regional['hu'] = {
    closeText: 'Bezárás',
    prevText: 'Vissza',
    nextText: 'Előre',
    currentText: 'ma',
    monthNames: ['január', 'február', 'március', 'április', 'május', 'június', 'július', 'augusztus', 'szeptember', 'október', 'november', 'december'],
    monthNamesShort: ['jan', 'feb', 'már', 'ápr', 'máj', 'jún', 'júl', 'aug', 'szep', 'okt', 'nov', 'dec'],
    dayNames: ['Vasámap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
    dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
    dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
    dateFormat: 'yy-mm-dd', firstDay: 1,
    isRTL: false};
  $.datepicker.setDefaults($.datepicker.regional['hu']);
});
