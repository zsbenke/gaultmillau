require 'test_helper'

class NewsletterSubscriptionTest < ActiveSupport::TestCase
  test "should not create a new subscription without email and full_name" do
    newsletter_subscription = NewsletterSubscription.new
    assert !newsletter_subscription.save, "created subscription without full_name and email"
  end

  test "should not create new subscription with wrong email format" do
    newsletter_subscription = NewsletterSubscription.new(:full_name => 'Full Name', :email => 'email@domain')
    assert !newsletter_subscription.save, "created subscription with wrong email address"
  end

  test "should create a token for subscription" do
    newsletter_subscription = NewsletterSubscription.new(:full_name => 'Full Name', :email => 'email@domain.com')
    newsletter_subscription.save
    assert !newsletter_subscription.token.nil?, "created subscription without token"
  end

  test "should not create new subscription for existing email address" do
    newsletter_subscription = NewsletterSubscription.new(:full_name => 'Full Name', :email => 'email@domain.com')
    newsletter_subscription.save

    same_newsletter_subscription = NewsletterSubscription.new(:full_name => 'Full Name', :email => 'email@domain.com')
    assert !same_newsletter_subscription.save, "created subscription with the same email address"
  end
end
