require 'test_helper'

class RestaurantReservationsControllerTest < ActionController::TestCase
  setup do
    @restaurant_reservation = restaurant_reservations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurant_reservations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant_reservation" do
    assert_difference('RestaurantReservation.count') do
      post :create, restaurant_reservation: { comment: @restaurant_reservation.comment, email: @restaurant_reservation.email, feedback_type: @restaurant_reservation.feedback_type, number_of_persons: @restaurant_reservation.number_of_persons, phone: @restaurant_reservation.phone, reservate_at: @restaurant_reservation.reservate_at }
    end

    assert_redirected_to restaurant_reservation_path(assigns(:restaurant_reservation))
  end

  test "should show restaurant_reservation" do
    get :show, id: @restaurant_reservation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restaurant_reservation
    assert_response :success
  end

  test "should update restaurant_reservation" do
    put :update, id: @restaurant_reservation, restaurant_reservation: { comment: @restaurant_reservation.comment, email: @restaurant_reservation.email, feedback_type: @restaurant_reservation.feedback_type, number_of_persons: @restaurant_reservation.number_of_persons, phone: @restaurant_reservation.phone, reservate_at: @restaurant_reservation.reservate_at }
    assert_redirected_to restaurant_reservation_path(assigns(:restaurant_reservation))
  end

  test "should destroy restaurant_reservation" do
    assert_difference('RestaurantReservation.count', -1) do
      delete :destroy, id: @restaurant_reservation
    end

    assert_redirected_to restaurant_reservations_path
  end
end
