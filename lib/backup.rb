#!/usr/bin/ruby

gm_backup_path = '~/backups/gm'
gmdb_backup_path = '~/backups/gmdb'
gm_app = "gaultmillau"
gmdb_app = "gmdb"

# Parse remote backup
capture_output = `heroku pg:backups public-url -q --app #{gm_app}`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: Preparing backup url for Gault&Millau"

# Download the backup file
gm_backup_file = File.join(gm_backup_path, "#{gm_app}_backup_#{t.strftime("%Y%m%d_%I%M")}.dump")
download = `curl -o #{gm_backup_file} "#{capture_output}"`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: download complete, saved to: #{gm_backup_file}"

# Compress the backup file
gzip = `gzip #{gm_backup_file}`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: backup complete"

# Parse remote backup
capture_output = `heroku pg:backups public-url -q --app #{gmdb_app}`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: Preparing backup url for GMDB"

# Download the backup file
gmdb_backup_file = File.join(gmdb_backup_path, "#{gmdb_app}_backup_#{t.strftime("%Y%m%d_%I%M")}.dump")
download = `curl -o #{gmdb_backup_file} "#{capture_output}"`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: download complete, saved to: #{gmdb_backup_file}"

# Compress the backup file
gzip = `gzip #{gmdb_backup_file}`
t = Time.now
puts "[#{t.strftime("%Y.%m.%d. %I:%M:%S.%L")}]: backup complete"
