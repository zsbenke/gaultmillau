namespace :admin do
  task :reset_comments_counter => :environment do
    ids = Set.new
    Comment.all.each {|c| ids << c.post_id}
    ids.each do |post_id|
      Post.reset_counters(post_id, :comments)
    end
  end

  task :clear_cache => :environment do
    Rails.cache.clear
  end

  task :clear_database => :environment do
    if ENV['GM_VERSION'] == 'Staging'
      posts = Post.where "created_at < ? AND category != 'oldalak' AND category != 'beagyazott'", 1.months.ago
      posts.destroy_all
    end
  end

  task :create_restaurant_reservation_block => :environment do
    hb = HomeBlock.new
    hb.kind = 'custom'
    hb.title = 'restaurant_reservation_block'
    hb.save
  end

  task :create_order_block => :environment do
    hb = HomeBlock.new
    hb.kind = 'custom'
    hb.title = 'order_block'
    hb.save
  end

  task :list_emails => :environment do
    dev_null = Logger.new("/dev/null")
    Rails.logger = dev_null
    ActiveRecord::Base.logger = dev_null
    User.confirmed.users.map { |u| puts u.email }
  end

  task :reset_search_caches => :environment do
    RestaurantSearchCache.reset_cache
    RestaurantSearchCache.update_ranks
  end

  task :migrate_favourites => :environment do
    favs = Favourite.all
    records = { 'restaurant' => Restaurant, 'hotel' => Hotel, 'winery' => Winery }

    favs.each do |fav|
      begin
        record = records[fav.kind].find(fav.record_id)
        if record.parent && record.parent.public
          fav.record_id = record.parent.id
          fav.save
        else
          Rails.logger.info("Favourite##{fav.id}: removed record, destroying favourite…")
          fav.destroy
        end
      rescue
        Rails.logger.info("Favourite##{fav.id}: missing record, destroying favourite…")
        fav.destroy
      end
    end
  end

  task :reprocess_attachments => :environment do
    Attachment.reprocess_attachments!
  end
end
