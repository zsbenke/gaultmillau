namespace :tasks do
  task :rename_order_statuses => :environment do
    orders = Order.where(:status => 'Átvett')
    orders.each do |order|
      order.status = 'Kézbesítő átvette'
      order.save
    end
  end

  task :assign_style_type_to_home_blocks => :environment do
    home_blocks = HomeBlock.all

    home_blocks.each do |home_block|
      home_block.style_type = "Alapértelmezett"
      home_block.save
    end
  end

  task :set_gm_guide_to_orders => :environment do
    product = Product.find(1)
    Order.update_all("product_id = #{product.id}")
  end

  task :rename_guides_category => :environment do
    posts = Post.where category: 'kalauz'
    posts.each do |p|
      p.category = 'kalauz-2014'
      p.save
    end
  end
end
