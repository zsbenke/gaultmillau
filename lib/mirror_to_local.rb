require 'fog'
require 'fileutils'

Excon.defaults[:write_timeout] = 300000

service = Fog::Storage.new({
    :provider            => 'Rackspace',
    :rackspace_username  => 'gaultmillauhu',
    :rackspace_api_key   => 'a7f55fe88c44b3877dbd444672bdc878',
    :persistent => false,
    :connection_options  => { :connect_timeout => 300000,
                              :read_timeout => 300000,
                              :write_timeout => 300000 }
})

container_name = ARGV[0]
backup_path = ARGV[1]
container = service.directories.get container_name
date = Time.now.strftime("%Y-%m-%d-%H-%M")

def log_to_file filename, message
  logs_path = "#{ARGV[1]}/#{ARGV[0]}/logs/"

  if !File.directory?(logs_path)
    FileUtils.mkdir_p logs_path
  end

  File.open("#{logs_path}/#{filename}", 'a') do |log_file|
    log_file.puts(message)
  end
end

container.files.each do |remote_file|
  if remote_file.content_type != 'application/directory'
    filename = "#{backup_path}/#{container_name}/#{remote_file.key}"
    directory = "#{backup_path}/#{container_name}/#{File.split(remote_file.key).first}"
    if !File.directory?(directory)
      FileUtils.mkdir_p directory
    end

    if !File.exists?(filename) || remote_file.content_length != File.size(filename)
      begin
        File.open(filename, 'w+') do |local_file|
          local_file.syswrite(remote_file.body)
        end
        puts "Downloaded #{filename}"
        log_to_file "backup-#{container_name}-#{date}.txt", "Downloaded #{filename}"
      rescue
        puts "Error downloading #{filename}"
      end
    else
      puts "Already downloaded #{filename}"
    end
  end
end
