require 'fog'

container = ARGV[0]

origin_service = Fog::Storage.new({
    :provider            => 'Rackspace',
    :rackspace_username  => 'wyctim',
    :rackspace_api_key   => 'ad9d4e4340a1f1194199f58e762f8081',
    :persistent => false,
    :connection_options  => { :connect_timeout => 7200,
                              :read_timeout => 7200,
                              :write_timeout => 7200 }
})

mirror_service = Fog::Storage.new({
    :provider            => 'Rackspace',
    :rackspace_username  => 'gaultmillauhu',
    :rackspace_api_key   => 'a7f55fe88c44b3877dbd444672bdc878',
    :persistent => false,
    :connection_options  => { :connect_timeout => 7200,
                              :read_timeout => 7200,
                              :write_timeout => 7200 }
})

origin = origin_service.directories.get container
mirror = mirror_service.directories.get container

origin.files.each do |file|
  mirror_head = mirror.files.head(file.key)

  if mirror_head && mirror_head.content_length == file.content_length
    puts "Already mirrored: #{file.key}"
  else
    mirror.files.create({ key: file.key, body: file.body }) ? puts("Mirrored: #{file.key}") : puts("Error with: #{file.key}")
  end
end
