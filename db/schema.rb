# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171108130328) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "attachments", force: true do |t|
    t.string   "title"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "category"
    t.text     "attachment_meta"
    t.string   "average_color"
    t.boolean  "attachment_processing"
  end

  create_table "blocks", force: true do |t|
    t.text     "content"
    t.integer  "post_id"
    t.integer  "position"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "kind"
    t.string   "css_class"
    t.integer  "attachment_id"
    t.integer  "restaurant_id"
  end

  add_index "blocks", ["post_id"], name: "index_blocks_on_post_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "favourites", force: true do |t|
    t.integer  "user_id"
    t.string   "kind"
    t.integer  "record_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "favourites", ["user_id"], name: "index_favourites_on_user_id", using: :btree

  create_table "feedbacks", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.text     "content"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "captcha"
    t.boolean  "is_error_reporting"
    t.string   "record_kind"
    t.string   "record_name"
    t.integer  "record_id"
  end

  create_table "home_blocks", force: true do |t|
    t.string   "kind"
    t.integer  "post_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.boolean  "has_featured_image"
    t.integer  "position"
    t.string   "image_align"
    t.text     "image_url"
    t.string   "title"
    t.string   "style_type"
  end

  add_index "home_blocks", ["post_id"], name: "index_home_blocks_on_post_id", using: :btree

  create_table "newsletter_subscriptions", force: true do |t|
    t.integer  "user_id"
    t.string   "full_name"
    t.string   "email"
    t.string   "token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "confirmed"
    t.datetime "confirmed_at"
  end

  add_index "newsletter_subscriptions", ["user_id"], name: "index_newsletter_subscriptions_on_user_id", using: :btree

  create_table "orders", force: true do |t|
    t.string   "email"
    t.string   "full_name"
    t.string   "city"
    t.string   "postcode"
    t.string   "address"
    t.string   "phone"
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "token"
    t.integer  "product_count"
    t.string   "billing_postcode"
    t.string   "billing_address"
    t.string   "billing_city"
    t.boolean  "billing_address_same_as_order_address"
    t.string   "billing_name"
    t.string   "billing_phone"
    t.string   "status"
    t.string   "post_identifier"
    t.boolean  "paid"
    t.text     "comment"
    t.string   "vat_number"
    t.string   "payment_method"
    t.string   "payment_token"
    t.integer  "product_id"
  end

  add_index "orders", ["product_id"], name: "index_orders_on_product_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "posts", force: true do |t|
    t.string   "title"
    t.integer  "user_id"
    t.string   "category"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "public",                default: false
    t.integer  "comments_count",        default: 0
    t.text     "excerpt"
    t.string   "url"
    t.datetime "published_at"
    t.integer  "attachment_id"
    t.boolean  "only_with_redeem_code"
    t.string   "post_class"
    t.boolean  "not_commentable"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name"
    t.integer  "price"
    t.integer  "free_delivery_from_quantity"
    t.integer  "delivery_price"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "available",                   default: true
    t.text     "image_url"
    t.text     "excerpt"
    t.boolean  "public",                      default: false
    t.string   "external_url"
  end

  create_table "progresses", force: true do |t|
    t.string   "queue",      limit: nil
    t.string   "job_name",   limit: nil
    t.string   "job_id",     limit: nil
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "redeem_codes", force: true do |t|
    t.string   "token"
    t.string   "year"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "redeem_codes", ["user_id"], name: "index_redeem_codes_on_user_id", using: :btree

  create_table "restaurant_reservations", force: true do |t|
    t.string   "book_at"
    t.string   "number_of_persons"
    t.string   "email"
    t.text     "comment"
    t.string   "phone"
    t.string   "feedback_type"
    t.integer  "user_id"
    t.integer  "restaurant_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "token"
    t.boolean  "save_phone_to_profile"
    t.boolean  "confirmed"
    t.datetime "confirmed_at"
    t.boolean  "save_to_users"
    t.string   "book_time"
    t.string   "name"
  end

  add_index "restaurant_reservations", ["restaurant_id"], name: "index_restaurant_reservations_on_restaurant_id", using: :btree
  add_index "restaurant_reservations", ["user_id"], name: "index_restaurant_reservations_on_user_id", using: :btree

  create_table "restaurant_search_caches", force: true do |t|
    t.string  "name"
    t.integer "rank"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["context"], name: "index_taggings_on_context", using: :btree
  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
  add_index "taggings", ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
  add_index "taggings", ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
  add_index "taggings", ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
  add_index "taggings", ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "role_admin",             default: false
    t.boolean  "role_editor",            default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "comments_count",         default: 0
    t.integer  "posts_count",            default: 0
    t.integer  "unread_comments"
    t.string   "phone"
    t.boolean  "role_accountant"
    t.boolean  "role_senior_editor"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
