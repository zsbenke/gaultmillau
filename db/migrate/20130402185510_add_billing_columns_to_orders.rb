class AddBillingColumnsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_name, :string
    add_column :orders, :billing_phone, :string
  end
end
