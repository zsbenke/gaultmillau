class AddAverageColorToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :average_color, :string
  end
end
