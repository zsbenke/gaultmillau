class CreateNewsletterSubscriptions < ActiveRecord::Migration
  def change
    create_table :newsletter_subscriptions do |t|
      t.belongs_to :user
      t.string :full_name
      t.string :email
      t.string :token

      t.timestamps
    end
    add_index :newsletter_subscriptions, :user_id
  end
end
