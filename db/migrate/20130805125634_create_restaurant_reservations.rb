class CreateRestaurantReservations < ActiveRecord::Migration
  def change
    create_table :restaurant_reservations do |t|
      t.datetime :book_at
      t.integer :number_of_persons
      t.string :email
      t.text :comment
      t.string :phone
      t.string :feedback_type
      t.belongs_to :user
      t.belongs_to :restaurant

      t.timestamps
    end
    add_index :restaurant_reservations, :user_id
    add_index :restaurant_reservations, :restaurant_id
  end
end
