class ChangeBookAtInRestaurantReservations < ActiveRecord::Migration
  def change
    change_column :restaurant_reservations, :book_at, :string
  end
end
