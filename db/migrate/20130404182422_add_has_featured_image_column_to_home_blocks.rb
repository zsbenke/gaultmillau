class AddHasFeaturedImageColumnToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :has_featured_image, :boolean
  end
end
