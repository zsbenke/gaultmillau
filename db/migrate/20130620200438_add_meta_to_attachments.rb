class AddMetaToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :attachment_meta, :text
  end
end
