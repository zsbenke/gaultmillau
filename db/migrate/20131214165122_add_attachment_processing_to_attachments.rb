class AddAttachmentProcessingToAttachments < ActiveRecord::Migration
  def change
    add_column :attachments, :attachment_processing, :boolean
  end
end
