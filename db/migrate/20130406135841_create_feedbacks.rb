class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.text :content

      t.timestamps
    end
  end
end
