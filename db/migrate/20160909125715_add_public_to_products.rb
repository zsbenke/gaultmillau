class AddPublicToProducts < ActiveRecord::Migration
  def change
    add_column :products, :public, :boolean, default: false

    Product.all.each do |p|
      p.public = true
      p.save
    end
  end
end
