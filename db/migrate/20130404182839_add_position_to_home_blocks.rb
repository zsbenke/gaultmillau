class AddPositionToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :position, :integer
  end
end
