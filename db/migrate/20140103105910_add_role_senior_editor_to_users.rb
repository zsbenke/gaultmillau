class AddRoleSeniorEditorToUsers < ActiveRecord::Migration
  def change
    add_column :users, :role_senior_editor, :boolean
  end
end
