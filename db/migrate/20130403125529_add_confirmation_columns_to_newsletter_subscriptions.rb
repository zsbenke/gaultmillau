class AddConfirmationColumnsToNewsletterSubscriptions < ActiveRecord::Migration
  def change
    add_column :newsletter_subscriptions, :confirmed, :boolean
    add_column :newsletter_subscriptions, :confirmed_at, :datetime
  end
end
