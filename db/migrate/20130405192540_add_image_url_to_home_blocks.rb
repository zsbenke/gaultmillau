class AddImageUrlToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :image_url, :text
  end
end
