class AddPermissionsColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :role_admin, :boolean, :default => false
    add_column :users, :role_editor, :boolean, :default => false
    add_column :users, :role_user, :boolean, :default => true
  end
end
