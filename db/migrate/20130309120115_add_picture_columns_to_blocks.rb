class AddPictureColumnsToBlocks < ActiveRecord::Migration
  def change
    add_column :blocks, :css_class, :string
    add_column :blocks, :attachment_id, :integer
  end
end
