class AddNotCommentableToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :not_commentable, :boolean
  end
end
