class AddSavePhoneToProfileToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :save_phone_to_profile, :boolean
  end
end
