class AddBookTimeToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :book_time, :string
  end
end
