class AddTokenToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :token, :string
  end
end
