class CreateBlocks < ActiveRecord::Migration
  def change
    create_table :blocks do |t|
      t.text :content
      t.belongs_to :post
      t.integer :position

      t.timestamps
    end
    add_index :blocks, :post_id
  end
end
