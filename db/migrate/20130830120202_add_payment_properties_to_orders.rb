class AddPaymentPropertiesToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :payment_method, :string
    add_column :orders, :status, :string
    add_column :orders, :post_identifier, :string
    add_column :orders, :paid, :boolean
    add_column :orders, :comment, :text
  end
end
