class AddStyleTypeToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :style_type, :string
  end
end
