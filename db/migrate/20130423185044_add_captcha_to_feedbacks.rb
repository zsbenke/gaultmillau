class AddCaptchaToFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :captcha, :string
  end
end
