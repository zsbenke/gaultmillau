class AddBillingAddressToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_postcode, :string
    add_column :orders, :billing_address, :string
    add_column :orders, :billing_city, :string
    add_column :orders, :billing_address_same_as_order_address, :boolean
  end
end
