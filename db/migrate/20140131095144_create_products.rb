class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :price
      t.integer :free_delivery_from_quantity
      t.integer :delivery_price
      t.text :description

      t.timestamps
    end
  end
end
