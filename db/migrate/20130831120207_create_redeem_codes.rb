class CreateRedeemCodes < ActiveRecord::Migration
  def change
    create_table :redeem_codes do |t|
      t.string :token
      t.string :year
      t.belongs_to :user

      t.timestamps
    end
    add_index :redeem_codes, :user_id
  end
end
