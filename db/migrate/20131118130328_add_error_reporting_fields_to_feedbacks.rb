class AddErrorReportingFieldsToFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :is_error_reporting, :boolean
    add_column :feedbacks, :record_kind, :string
    add_column :feedbacks, :record_name, :string
    add_column :feedbacks, :record_id, :integer
  end
end
