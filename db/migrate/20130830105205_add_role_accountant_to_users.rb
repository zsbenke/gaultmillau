class AddRoleAccountantToUsers < ActiveRecord::Migration
  def change
    add_column :users, :role_accountant, :boolean
  end
end
