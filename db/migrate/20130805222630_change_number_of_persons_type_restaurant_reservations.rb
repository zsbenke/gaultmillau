class ChangeNumberOfPersonsTypeRestaurantReservations < ActiveRecord::Migration
  def change
    change_column :restaurant_reservations, :number_of_persons, :string
  end
end
