class AddOnlyWithRedeemCodeToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :only_with_redeem_code, :boolean
  end
end
