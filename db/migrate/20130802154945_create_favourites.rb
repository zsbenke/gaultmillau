class CreateFavourites < ActiveRecord::Migration
  def change
    create_table :favourites do |t|
      t.belongs_to :user
      t.string :kind
      t.integer :record_id

      t.timestamps
    end
    add_index :favourites, :user_id
  end
end
