class CreateRestaurantSearchCache < ActiveRecord::Migration
  def change
    create_table :restaurant_search_caches do |t|
      t.string :name
      t.integer :rank
    end
  end
end
