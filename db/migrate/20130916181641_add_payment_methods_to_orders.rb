class AddPaymentMethodsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :payment_method, :string
    add_column :orders, :payment_token, :string
  end
end
