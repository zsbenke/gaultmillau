class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :email
      t.string :full_name
      t.string :city
      t.string :postcode
      t.string :address
      t.string :phone
      t.belongs_to :user

      t.timestamps
    end
    add_index :orders, :user_id
  end
end
