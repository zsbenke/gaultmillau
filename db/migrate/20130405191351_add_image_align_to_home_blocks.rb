class AddImageAlignToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :image_align, :string
  end
end
