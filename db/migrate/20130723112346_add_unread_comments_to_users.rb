class AddUnreadCommentsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :unread_comments, :integer
  end
end
