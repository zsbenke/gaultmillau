class AddConfirmationFieldsToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :confirmed, :boolean
    add_column :restaurant_reservations, :confirmed_at, :datetime
  end
end
