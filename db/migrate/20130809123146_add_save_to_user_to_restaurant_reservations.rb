class AddSaveToUserToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :save_to_users, :boolean
  end
end
