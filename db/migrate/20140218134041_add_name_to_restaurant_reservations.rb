class AddNameToRestaurantReservations < ActiveRecord::Migration
  def change
    add_column :restaurant_reservations, :name, :string
  end
end
