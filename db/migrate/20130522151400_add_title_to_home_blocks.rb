class AddTitleToHomeBlocks < ActiveRecord::Migration
  def change
    add_column :home_blocks, :title, :string
  end
end
