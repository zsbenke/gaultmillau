class CreateHomeBlocks < ActiveRecord::Migration
  def change
    create_table :home_blocks do |t|
      t.string :kind
      t.belongs_to :post

      t.timestamps
    end
    add_index :home_blocks, :post_id
  end
end
