class RemoveRoleUserColumnFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :role_user
  end
end
