class AddImageUrlToProducts < ActiveRecord::Migration
  def change
    add_column :products, :image_url, :text
    add_column :products, :excerpt, :text
  end
end
