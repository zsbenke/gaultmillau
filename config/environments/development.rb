Gaultmillau::Application.configure do
  config_path = File.expand_path(Rails.root.to_s + '/config/settings.yml')
  if File.exists? config_path
    ENV.update YAML.load_file(config_path)[Rails.env]
  end
  # Settings specified here will take precedence over those in config/application.rb
  config.action_controller.default_url_options = { host: 'http://gaultmillau.test' }
  config.action_mailer.default_url_options = { host: 'http://gaultmillau.test' }

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching
  config.consider_all_requests_local = true
  config.cache_store = :dalli_store

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Do not compress assets
  config.assets.compress = false

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Mailer
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.delivery_method = :test

  config.paperclip_defaults = {
    storage: :fog,
    path: ":attachment/:id/:style/:filename",
    fog_credentials: {
      provider: "Local",
      local_root: "#{Rails.root}/public"
    },
    fog_directory: "system",
    fog_host: "http://gaultmillau.hu/system"
  }

  config.assets.paths << Rails.root.join('public', 'sprites')
end
