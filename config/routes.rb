Gaultmillau::Application.routes.draw do
  get "/current-user-meta" => "home#current_user_meta", :as => "home_current_user_meta"

  namespace :admin do
    resources :posts, :path => 'tartalommenedzser' do
      resources :blocks
      get :autocomplete_tag_name, :on => :collection
    end

    resources :home_blocks, :path => 'cimlapszerkeszto'
    resources :comments, :path => 'hozzaszolasok'
    resources :orders, :path => 'rendelesek', :only => [:index, :edit, :update, :destroy]
    resources :restaurant_reservations, :path => 'asztalfoglalasok', :only => [:index, :show, :destroy]
    resources :tags, :path => 'cimkek', :only => [:index, :destroy, :update, :edit, :show]
    resources :attachments, :path => 'mellekletek'
    resources :products, :path => 'termekek'
    resources :users, :path => 'felhasznalok' do
      member do
        get 'manual_confirm'
      end
    end

    post "/admin/home_blocks/sort" => "home_blocks#sort", :as => "sort_home_blocks"
    get "/admin/posts/:id/edit_post_meta" => "posts#edit_post_meta", :as => "edit_post_meta"
    post "/admin/blocks/sort" => "blocks#sort", :as => "sort_blocks"
    get "/admin/js/recipes_list" => "posts#recipes_list", :as => "recipes_list"
  end

  get "/admin" => "admin#index", :as => "admin_index"

  devise_for :users,
             :path => 'felhasznalok',
             :path_names => {
               :sign_up => 'regisztracio',
               :sign_in => 'bejelentkezes',
               :sign_out => 'kijelentkezes'
             }, :controllers => { :registrations => "registrations" }

  resources :products, :path => 'termekek', :only => [:show, :index] do
    resources :orders, :path => 'rendelesek', :only => [:create, :new, :index]
  end

  get 'rendelesek/:token', to: 'orders#show', as: :order

  resources :newsletter_subscriptions, :path => 'hirlevel'
  get 'hirlevel/jovahagyas/:id', to: 'newsletter_subscriptions#confirm', :as => 'confirm_newsletter_subscription'
  get 'hirlevel/leiratkozas/:id', to: 'newsletter_subscriptions#confirm_unsubscribe', :as => 'confirm_newsletter_unsubscribe'

  resources :feedbacks, :path => 'kapcsolat', :only => [:create]
  get 'kapcsolat', to: 'feedbacks#new', :as => 'new_feedback'

  resources :favourites, :path => 'kedvencek', :only => [:create, :update]

  get '/ettermek/legjobbak', to: 'restaurants#best_of_map', as: :best_of_restaurants_map
  get '/szallodak/legjobbak', to: 'hotels#best_of_map', as: :best_of_hotels_map
  get '/boraszatok/legjobbak', to: 'wineries#best_of_map', as: :best_of_wineries_map
  get '/ettermek/budapest-extra', to: 'restaurants#additional', as: :additional

  resources :restaurants, :path => 'ettermek' do
    get :autocomplete_restaurant_search_cache_name, :on => :collection
  end

  resources :hotels, :path => 'szallodak'
  resources :wineries, :path => 'boraszatok'

  resources :amuva_posts, :path => 'muvelt-alkoholista' do
    resources :amuva_comments
  end

  resources :bsz_posts, :path => 'buvos-szakacs' do
    resources :bsz_comments
  end

  resources :mh_posts, :path => 'mese-habbal' do
    resources :mh_comments
  end

  resources :news, :path => 'gm-magazin' do
    resources :news_comments
  end

  resources :restaurant_reservations, :path => 'ettermek/asztalfoglalas', :only => [:index, :new, :create]
  get 'asztalfoglalas/jovahagyas/:id', to: 'restaurant_reservations#confirm', :as => 'confirm_restaurant_reservation'

  get '/muvelt-alkoholista/cimke/:tag', to: 'amuva_posts#index', as: :amuva_tag
  get '/buvos-szakacs/cimke/:tag', to: 'bsz_posts#index', as: :bsz_tag
  get '/gm-magazin/cimke/:tag', to: 'news#index', as: :news_tag
  get '/mese-habbal/cimke/:tag', to: 'mh_posts#index', as: :mh_tag

  get '/feed', to: 'home#feed', as: :home_feed, :defaults => { :format => 'rss' }
  get '/feed/muvelt-alkoholista', to: 'amuva_posts#index', as: :amuva_feed, :defaults => { :format => 'rss' }
  get '/feed/muvelt-alkoholista/kommentek', to: 'amuva_comments#index', as: :amuva_comments_feed, :defaults => { :format => 'rss' }
  get '/feed/buvos-szakacs', to: 'bsz_posts#index', as: :bsz_feed, :defaults => { :format => 'rss' }
  get '/feed/buvos-szakacs/kommentek', to: 'bsz_comments#index', as: :bsz_comments_feed, :defaults => { :format => 'rss' }
  get '/feed/mese-habbal', to: 'mh_posts#index', as: :mh_feed, :defaults => { :format => 'rss' }
  get '/feed/mese-habbal/kommentek', to: 'mh_comments#index', as: :mh_comments_feed, :defaults => { :format => 'rss' }

  resources :recipes, :path => 'receptek'

  get '/kereses', to: 'search#index', as: :search

  # Redirects
  get '/c/:id', to: 'short_urls#show', :as => 'short_url'
  get '/reg', to: redirect('felhasznalok/regisztracio')
  get '/regisztracio', to: redirect('felhasznalok/regisztracio')
  get '/bejelentkezes', to: redirect('felhasznalok/bejelentkezes')
  get '/fiok', to: redirect('felhasznalok/edit')
  get '/fiokom', to: redirect('felhasznalok/edit')
  get '/kod', to: redirect('a-gault-and-millau-2015-os-kalauz-nyomtatott-kuponkodjainak-bevaltasa')

  # Root and Pages

  get '/kalauz-2012/', to: 'guides2012#index', as: :guides2012
  get '/kalauz-2012/:id', to: 'guides2012#show', as: :guide2012
  get '/kalauz-2013/', to: 'guides2013#index', as: :guides2013
  get '/kalauz-2013/:id', to: 'guides2013#show', as: :guide2013
  get '/kalauz-2014/', to: 'guides2014#index', as: :guides2014
  get '/kalauz-2014/:id', to: 'guides2014#show', as: :guide2014
  get '/kalauz-2015/', to: 'guides2015#index', as: :guides2015
  get '/kalauz-2015/:id', to: 'guides2015#show', as: :guide2015
  get '/kalauz-2016/', to: 'guides2016#index', as: :guides2016
  get '/kalauz-2016/:id', to: 'guides2016#show', as: :guide2016
  get '/kalauz-2017/', to: 'guides2017#index', as: :guides2017
  get '/kalauz-2017/:id', to: 'guides2017#show', as: :guide2017
  get ':id', to: 'pages#show', as: :page

  root :to => "home#index"
end
