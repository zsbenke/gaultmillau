every 1.day, at: '1am' do
  runner 'Backup.create'
end

every 1.day, at: '2am' do
  runner 'Backup.restore_restaurant_database'
end

every 1.day, at: '4pm' do
  rake 'admin:reset_search_caches'
end

every 3.hours do
  command 'bin/delayed_job restart'
end
